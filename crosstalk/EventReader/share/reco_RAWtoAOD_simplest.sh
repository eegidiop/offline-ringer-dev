#!/bin/sh
############## Batch Mode on LXPLUS ###############
# setupATLAS;
# asetup AtlasProduction,20.1.4.1;

############## Batch Mode on LXPLUS ###############

# asetup Athena,22.0.44;
shopt -s expand_aliases
source $HOME/.bashrc

setupATLAS
asetup Athena,21.0.30

nEvents=100
dataset=/eos/user/m/mhufnage/ALP_project/RAW/user.mhufnage.data17_13TeV.00329542.physics_MinBias.daq.RAW_der1655905167data17_13TeV.00329542.physics_MinBias.daq.RAW._lb0239._SFO-4._0001.data
indexOutput=0
# for i in {000002..000003}
for file in $dataset #`cat $dataset`
    do
        echo $file

        echo "RAWtoESD...$indexOutput"
        Reco_tf.py --AMIConfig=r9738 --AMITag=r9738   --AddCaloDigi=True --outputESDFile=ESD_data_$indexOutput.pool.root --inputBSFile=$file --maxEvents $nEvents --postExec="RAWtoESD:StreamESD.ItemList+=[\"TileDigitsContainer#TileDigitsCnt\"]; StreamESD.ItemList+=[\"LArDigitContainer#*\"]; StreamESD.ItemList+=[\"LArRawChannelContainer#LArRawChannels\"]; StreamESD.ItemList+=[\"TileRawChannelContainer#TileRawChannelCnt\"] " >Reco_tf_RDOtoESD_$indexOutput.log 2>&1

        echo "ESDtoAOD...$indexOutput"
        Reco_tf.py --AMIConfig=r9738 --AMITag=r9738   --AddCaloDigi=True --outputAODFile=AOD_data_$indexOutput.pool.root --inputESDFile=ESD_data_$indexOutput.pool.root --maxEvents $nEvents --postExec="ESDtoAOD:StreamAOD.ItemList+=[\"TileDigitsContainer#TileDigitsCnt\"]; StreamAOD.ItemList+=[\"LArDigitContainer#*\"]; StreamAOD.ItemList+=[\"LArRawChannelContainer#LArRawChannels\"]; StreamAOD.ItemList+=[\"TileRawChannelContainer#TileRawChannelCnt\"];StreamAOD.ItemList+=[\"CaloClusterCellLinkContainer#*\"] " >Reco_tf_ESDtoAOD_$indexOutput.log 2>&1

        indexOutput=$((indexOutput+1))
    done

