#!/bin/sh
# shopt -s expand_aliases
# source $HOME/.bashrc
############## Batch Mode on LXPLUS ###############

# asetup Athena,22.0.44;
shopt -s expand_aliases
source $HOME/.bashrc
setupATLAS

nEvents=500
# dataset=/eos/user/m/mhufnage/ALP_project/HITS/mc16_Zee/mc16_13TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.simul.HITS.e3601_e5984_s3240/HITS*.root.1
dataset=/eos/user/m/mhufnage/ALP_project/EVNT/mc16_13TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.merge.EVNT.e3601_e5984/EVNT.12228488._000014.pool.root.1
indexOutput=0
# for i in {000002..000003}
for file in $dataset #`cat $dataset`
    do
        echo $file

        echo "EVNTtoHITS...$indexOutput"

        asetup AtlasOffline,21.0.15
        Sim_tf.py --AMIConfig s3240 --AMITag s3240 --preInclude=SimulationJobOptions/preInclude.VertexPosOff.py --inputEvgenFile=$file --outputHitsFile=HITS_Zee_$indexOutput.pool.root --maxEvents=$nEvents > EVNTtoHITS_$indexOutput.log 2>&1 # posVertexOff
        # Sim_tf.py --AMIConfig s3240 --AMITag s3240 --inputEvgenFile=$file --outputHitsFile=HITS_Zee_$indexOutput.pool.root --maxEvents=$nEvents > EVNTtoHITS_$indexOutput.log 2>&1 # standard PosVertex

        #  HITStoRDO...
        echo "HITStoRDO...$indexOutput" # set number of digits in MC from 5 to 4.
        asetup Athena,22.0.44
        Reco_tf.py  --athenaopts=' --threads=1 --imf --perfmon' --AddCaloDigi True --maxEvents $nEvents --inputHitsFile=HITS_Zee_$indexOutput.pool.root --preExec="HITtoRDO:from LArROD.LArRODFlags import larRODFlags;larRODFlags.nSamples.set_Value(4)" --postExec="HITtoRDO:streamRDO.ItemList+=[\"LArHitContainer#LArHitEMB\"];streamRDO.ForceRead=True" --outputRDOFile=RDO_Zee_$indexOutput.pool.root >Reco_tf_HITStoRDO_$indexOutput.log 2>&1
        
        echo "RDOtoESD...$indexOutput"
        Reco_tf.py  --athenaopts=' --threads=1 --imf --perfmon' --AddCaloDigi=True --outputESDFile=ESD_Zee_$indexOutput.pool.root --inputRDOFile=RDO_Zee_$indexOutput.pool.root --maxEvents $nEvents --postExec="RAWtoESD:StreamESD.ItemList+=[\"TileDigitsContainer#TileDigitsCnt\"]; StreamESD.ItemList+=[\"LArDigitContainer#*\"]; StreamESD.ItemList+=[\"LArRawChannelContainer#LArRawChannels\"]; StreamESD.ItemList+=[\"TileRawChannelContainer#TileRawChannelCnt\"];StreamESD.ItemList+=[\"LArHitContainer#LArHitEMB\"] " >Reco_tf_RDOtoESD_$indexOutput.log 2>&1

        echo "ESDtoAOD...$indexOutput"
        Reco_tf.py  --athenaopts=' --threads=1 --imf --perfmon' --AddCaloDigi=True --outputAODFile=AOD_Zee_$indexOutput.pool.root --inputESDFile=$file --maxEvents $nEvents --postExec="ESDtoAOD:StreamAOD.ItemList+=[\"TileDigitsContainer#TileDigitsCnt\"]; StreamAOD.ItemList+=[\"LArDigitContainer#*\"]; StreamAOD.ItemList+=[\"LArRawChannelContainer#LArRawChannels\"]; StreamAOD.ItemList+=[\"TileRawChannelContainer#TileRawChannelCnt\"];StreamAOD.ItemList+=[\"LArHitContainer#LArHitEMB\"];StreamAOD.ItemList+=[\"CaloClusterCellLinkContainer#*\"] " >Reco_tf_ESDtoAOD_$indexOutput.log 2>&1

        # rm RDO_Zee_$indexOutput.pool.root

        indexOutput=$((indexOutput+1))
    done