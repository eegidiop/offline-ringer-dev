#!/bin/sh
# Get the LAr OFC to NTuple 
# https://acode-browser1.usatlas.bnl.gov/lxr/source/athena/LArCalorimeter/LArCalibTools/share/LArCommConditions2Ntuple.py

 
###########################
# Tested Working commands #
###########################

## OFC OK
setupATLAS
asetup Athena,22.0.70
athena.py -c 'RootFile="LArCalib_OFC_r329542.root"; RunNumber=329542; Objects=["OFC"];SuperCells=False;IsMC=False;IsFlat=True' LArCalibTools/LArCommConditions2Ntuple.py
# athena.py -c 'RootFile="LArCalib_OFCMinBias_r329542.root"; RunNumber=329542; Objects=["OFC","MINBIAS"];SuperCells=False;IsMC=False;IsFlat=True' LArCalibTools/LArCommConditions2Ntuple.py
# athena.py -c 'PoolFiles="data17_13TeV.00329542.physics_MinBias.daq.RAW";RootFile="LArCalib_OFC_r329542.root"; RunNumber=329542; Objects=["OFC"];SuperCells=False;IsMC=False;IsFlat=True' LArCalibTools/LArCommConditions2Ntuple.py

## Pedestal OK
setupATLAS
asetup Athena,22.0.70
athena.py -c 'RootFile="LArOFC_ped_r329542.root"; RunNumber=329542;Objects=["PEDESTAL"];SuperCells=False;IsMC=False;IsFlat=True' LArCalibTools/LArCommConditions2Ntuple.py ## OK

## M_phys / M_calib OK
setupATLAS
asetup Athena,22.0.44
athena.py -c 'RootFile="LArCalib_mphysovermcalib_r329542.root"; RunNumber=329542; Objects=["MPHYSOVERMCAL"];SuperCells=False;IsMC=False;IsFlat=True' LArCalibTools/LArCommConditions2Ntuple.py # ok

## UA2MEV #OK, but empty DAC2uA
setupATLAS
asetup Athena,22.0.44
athena.py -c 'RootFile="LArCalib_ua2mev329542_athena22044.root"; RunNumber=329542; Objects=["UA2MEV"];SuperCells=False;IsMC=False;IsFlat=True' LArCalibTools/LArCommConditions2Ntuple.py  #OK, but empty DAC2uA

## DAC2uA (https://cds.cern.ch/record/942528)
# F_dac->uA (uA/DAC): (gain independent quantity)
#   PSB-E               : 0.002284
#   Strips              : 0.0025464
#   Middle (eta < 0.8)  : 0.150127 
#   Middle (eta > 0.8)  : 0.0751078 
#   Back                : 0.0751078

## Ramp OK
setupATLAS
asetup Athena,22.0.44
athena.py -c 'RootFile="LArOFC_ramp329542_athena22044.root"; RunNumber=329542;Objects=["RAMP"];SuperCells=False;IsMC=False;IsFlat=True' LArCalibTools/LArCommConditions2Ntuple.py
