import ROOT
import numpy as np
import math

def plotTH2F(h,xlabel,ylabel,cName='canvas',logy=False,plotType='colz'):
    c = ROOT.TCanvas(cName)
    c.SetLogy(logy)
    c.Draw()
    h.Draw(plotType)
    h.GetYaxis().SetTitle(ylabel)
    h.GetXaxis().SetTitle(xlabel)
    # h.SetMinimum(setMin)
    return c


def plotTH1F(h,xlabel,ylabel,cName='canvas',logy=True,plotType='hpe',setStats=True, setMin=0.1):
    c = ROOT.TCanvas(cName)
    c.SetLogy(logy)
    c.Draw()
    h.Draw(plotType)
    h.GetYaxis().SetTitle(ylabel)
    h.GetXaxis().SetTitle(xlabel)
    h.SetMinimum(setMin)
    if not(setStats):
        h.SetStats(0)
    return c

def histTH2F(name,title,nbins1,xlow1,xhigh1,nbins2,xlow2,xhigh2,contour=100):
    h = ROOT.TH2F(name,title,nbins1,xlow1,xhigh1,nbins2,xlow2,xhigh2)
    h.SetDirectory(0)
    h.SetStats(0)
    #ROOT.gStyle().SetPallete(ROOT.kRainBow)
    h.SetContour(contour)
    return h

def histTH1F(name,title,nbins,xlow,xhigh):
    h = ROOT.TH1F(name,title,nbins,xlow,xhigh)
    h.SetDirectory(0)   
    return h

def histTH1D(name,title,nbins,xlow,xhigh):
    h = ROOT.TH1D(name,title,nbins,xlow,xhigh)
    h.SetDirectory(0)   
    return h

def histLayer(layersList, dim='eta', hLim=0.3, hBins=20):
    hList = []
    for lay in range(0, len(layersList)):
        h = ROOT.TH1F(dim+"Layer %d"%(lay),dim+"%s"%(layersList[lay]),hBins, -hLim, hLim)
        h.SetDirectory(0) #dont add to the internal registry
        hList.append( h )
    return hList



def histLayerRegionFixBin(dictCalo, dim='eta', name='test',hBins=30, hLim1=10, hLim2=10 ):
    hList = []
    # hList = np.empty(np.shape(dictCalo['Layer'])[0], dtype=object)
    # hList[...] = [[] for _ in range(hList.shape[0])]
    layersList = dictCalo['Layer']
    # if dim == 'eta':
    RegionMinList = dictCalo['etaLowLim']
    RegionMaxList = dictCalo['etaHighLim']
    detaList = dictCalo['granularityEtaLayer']
    dphiList = dictCalo['granularityPhiLayer']

    for lay in range(0, len(layersList)):
        layList = []
        maxEta = np.array(RegionMaxList[lay])
        minEta = np.array(RegionMinList[lay])
        deta   = np.array(     detaList[lay])
        dphi   = np.array(     dphiList[lay])
        for etaRegion in range(0, np.size(minEta)):
            # Calculating the bin number
            if dim=='eta':
                h = ROOT.TH1F("%s_layer%d_reg%d"%(name,lay,etaRegion),
                            "%s - %.3f < |%s| < %.3f - %s"%(name, minEta[etaRegion],dim,maxEta[etaRegion],layersList[lay]),
                            hBins, 
                            hLim1, 
                            hLim2)
                
            if dim=='phi':
                h = ROOT.TH1F("%s_layer%d_reg%d"%(name,lay,etaRegion),
                            "%s - %.3f < |%s| < %.3f - %s"%(name, minEta[etaRegion],dim,maxEta[etaRegion],layersList[lay]),
                            hBins, 
                            hLim1, 
                            hLim2)
            h.SetDirectory(0) #dont add to the internal registry
            layList.append( h )
        # print(np.shape(layList), layList)
        hList.append( layList )
        # print(np.shape(hList),hList[lay])
        # hList[lay] = layList
        
    return hList 

# changed to get bothSides when useRegionLimits=False
def histLayerDynBin(dictCalo, dim='eta', hLim=0.3, useRegionLimits=False, bothSides=False, negSide=False, divBinEta=1, divBinPhi=1, histNameAlias=''):
    hList = []
    # hList = np.empty(np.shape(dictCalo['Layer'])[0], dtype=object)
    # hList[...] = [[] for _ in range(hList.shape[0])]
    layersList = dictCalo['Layer']
    # if dim == 'eta':
    RegionMinList   = dictCalo['etaLowLim']
    RegionMaxList   = dictCalo['etaHighLim']
    detaList        = dictCalo['granularityEtaLayer']
    dphiList        = dictCalo['granularityPhiLayer']
    detaPreciseList = dictCalo['granularityEtaLayerPrecise']
    dphiPreciseList = dictCalo['granularityPhiLayerPrecise']
    if (bothSides==True) and (useRegionLimits==False):
        regStrEta   = "|eta|" # title of the histogram
        regStrPhi   = "|phi|"
        regWidth    = 2 # if selecting both regions, the width will be 2 times the single side width (impact at the number of cells/bin)
    else:
        regStrEta   = "eta"
        regStrPhi   = "phi"
        regWidth    = 1 # single side selection

    for lay in range(0, len(layersList)):
        layList = []
        maxEta = np.array(RegionMaxList[lay])
        minEta = np.array(RegionMinList[lay])
        # Get precise granularity for every layer, but not for Forward. In forward, there are many granularity values, that will add new complexities to this task.
        if layersList[lay] != "FCAL0" or layersList[lay] != "FCAL1" or layersList[lay] != "FCAL2": # if layer is not forward, get precise values of granularity
            deta    = np.array(     detaPreciseList[lay])
            dphi    = np.array(     dphiPreciseList[lay])
        else:
            deta    = np.array(     detaList[lay])
            dphi    = np.array(     dphiList[lay])
        # print(deta)

        for etaRegion in range(0, np.size(minEta)):
            # print( "%.3f < %s < %.3f - %s"%(minEta[etaRegion],dim,maxEta[etaRegion],layersList[lay]))
            
            # Calculating the bin number
            if dim=='eta':
                regionSize = regWidth * (maxEta[etaRegion] - minEta[etaRegion]) # the size of region in eta
                hBins =  int(round( (regionSize/deta[etaRegion]) / divBinEta)) - 1 #the division bin value is the number of cells per bin, given the total region width divided by its granularity value
                # print("Dim: {}, regionSize: {}, deta[etaRegion]: {}, round((regionSize/dphi[etaRegion]): {} ,Bins: {}".format(dim, regionSize, deta[etaRegion], round(regionSize/deta[etaRegion]), hBins))
                # hBins = int(round(nCellsHalfRegion))
                if hBins < 5: # just an arbitrary number of the minimum number of bins for each region
                    hBins = 5
                    # print("\tNumber of bins is setted to {}.".format(hBins))
                if useRegionLimits:
                    if negSide:
                        hLim1 = -maxEta[etaRegion]
                        hLim2 = -minEta[etaRegion]
                    else:
                        hLim1 = minEta[etaRegion]
                        hLim2 = maxEta[etaRegion]
                else:
                    hLim1 = -hLim
                    hLim2 = hLim
                h = ROOT.TH1F(histNameAlias+dim+"_layer%d_reg%d"%(lay,etaRegion),
                            "ETA in %.3f < %s < %.3f - %s"%(hLim1,regStrEta,hLim2,layersList[lay]),
                            hBins, 
                            hLim1, 
                            hLim2)
                # leg = ROOT.TLegend(0.7 ,0.6 ,0.85 ,0.75)
                # leg.AddEntry(h,"D"+dim+" = %.4f"%(deta[etaRegion])+" Layer %d"%(lay))
                h.GetXaxis().SetTitle("\Delta\eta")
                h.GetYaxis().SetTitle("Number of events")
                # h.SetMinimum(0.1)
                h.SetMinimum(0)
                # binsWidth   = h.GetXaxis().GetBinWidth(1)
                # print(h.GetBinCenter(1), h.GetXaxis().GetBinWidth(1))
            if dim=='phi':
                regionSize = regWidth * 1*math.pi#3.1415
                hBins =  int(round((regionSize/dphi[etaRegion]) / divBinPhi)) - 1
                # print("Dim: {}, regionSize: {}, dphi[etaRegion]: {}, round((regionSize/dphi[etaRegion]): {} ,Bins: {}".format(dim, regionSize, dphi[etaRegion], round(regionSize/dphi[etaRegion]), hBins))
                # hBins = int(round(nCellsHalfRegion))
                if hBins < 8:
                    hBins = 8
                if useRegionLimits:
                    if negSide:
                        hLim1 = round(-1*math.pi, 4)#3.14
                        hLim2 = 0
                        hLim1_eta = -maxEta[etaRegion]
                        hLim2_eta = -minEta[etaRegion]
                    else:
                        hLim1 = 0
                        hLim2 = round(1*math.pi, 4)#3.14
                        hLim1_eta = minEta[etaRegion]
                        hLim2_eta = maxEta[etaRegion]
                    h = ROOT.TH1F(histNameAlias+dim+"_layer%d_reg%d"%(lay,etaRegion),
                                "PHI in %.3f < %s < %.3f - %s"%(hLim1_eta,regStrEta,hLim2_eta,layersList[lay]),
                                hBins,
                                hLim1,
                                hLim2)
                    # leg = ROOT.TLegend(0.7 ,0.6 ,0.85 ,0.75)
                    # leg.AddEntry(h,"D"+dim+" = %.4f"%(dphi[etaRegion])+" Layer %d"%(lay))
                    h.GetXaxis().SetTitle('\phi')
                    h.GetYaxis().SetTitle("Number of events")
                    # h.SetMinimum(0.1)
                    h.SetMinimum(0)
                else:
                    hLim1 = -hLim
                    hLim2 = hLim
                    h = ROOT.TH1F(histNameAlias+dim+"_layer%d_reg%d"%(lay,etaRegion),
                                    "DPHI in %.3f < %s < %.3f - %s"%(hLim1,regStrPhi,hLim2,layersList[lay]),
                                    hBins, 
                                    hLim1, 
                                    hLim2)
                    # leg = ROOT.TLegend(0.7 ,0.6 ,0.85 ,0.75)
                    # leg.AddEntry(h,"D"+dim+" = %.4f"%(dphi[etaRegion])+" Layer %d"%(lay))
                    h.GetXaxis().SetTitle('\Delta\phi')
                    h.GetYaxis().SetTitle("Number of events")
                    h.SetMinimum(0)
            h.SetDirectory(0) #dont add to the internal registry
            layList.append( h )
            # print("Dim: {}, Layer: {}.{}, Bins: {}".format(dim, lay, etaRegion, hBins))
        # print(np.shape(layList), layList)
        hList.append( layList )
        # print(np.shape(hList),hList[lay])
        # hList[lay] = layList
    return hList 

# def histLayerDynBin(dictCalo, dim='eta', hLim=0.3, useRegionLimits=False, negSide=False, divBinEta=1, divBinPhi=1, histNameAlias=''):
#     hList = []
#     # hList = np.empty(np.shape(dictCalo['Layer'])[0], dtype=object)
#     # hList[...] = [[] for _ in range(hList.shape[0])]
#     layersList = dictCalo['Layer']
#     # if dim == 'eta':
#     RegionMinList = dictCalo['etaLowLim']
#     RegionMaxList = dictCalo['etaHighLim']
#     detaList = dictCalo['granularityEtaLayer']
#     dphiList = dictCalo['granularityPhiLayer']

#     for lay in range(0, len(layersList)):
#         layList = []
#         maxEta = np.array(RegionMaxList[lay])
#         minEta = np.array(RegionMinList[lay])
#         deta   = np.array(     detaList[lay])
#         dphi   = np.array(     dphiList[lay])
#         # print(deta)

#         for etaRegion in range(0, np.size(minEta)):
#             # print( "%.3f < %s < %.3f - %s"%(minEta[etaRegion],dim,maxEta[etaRegion],layersList[lay]))
            
#             # Calculating the bin number
#             if dim=='eta':
#                 regionSize = (maxEta[etaRegion] - minEta[etaRegion])
#                 hBins =  int(round(regionSize/deta[etaRegion] / divBinEta ))
#                 # hBins = int(round(nCellsHalfRegion))
#                 if hBins < 10:
#                     hBins = 10
#                 if useRegionLimits:
#                     if negSide:
#                         hLim1 = -maxEta[etaRegion]
#                         hLim2 = -minEta[etaRegion]
#                     else:
#                         hLim1 = minEta[etaRegion]
#                         hLim2 = maxEta[etaRegion]
#                 else:
#                     hLim1 = -hLim
#                     hLim2 = hLim
#                 h = ROOT.TH1F(histNameAlias+dim+"_layer%d_reg%d"%(lay,etaRegion),
#                             "ETA in %.3f < %s < %.3f - %s"%(hLim1,"eta",hLim2,layersList[lay]),
#                             hBins, 
#                             hLim1, 
#                             hLim2)
#                 # leg = ROOT.TLegend(0.7 ,0.6 ,0.85 ,0.75)
#                 # leg.AddEntry(h,"D"+dim+" = %.4f"%(deta[etaRegion])+" Layer %d"%(lay))
#                 h.GetXaxis().SetTitle("\eta")
#                 h.GetYaxis().SetTitle("Number of events")
#                 h.SetMinimum(0)
#             if dim=='phi':
#                 regionSize = 3.1415
#                 hBins =  int(round(regionSize/dphi[etaRegion] / divBinPhi ))
#                 # hBins = int(round(nCellsHalfRegion))
#                 if hBins < 8:
#                     hBins = 8
#                 if useRegionLimits:
#                     if negSide:
#                         hLim1 = -3.14
#                         hLim2 = 0
#                         hLim1_eta = -maxEta[etaRegion]
#                         hLim2_eta = -minEta[etaRegion]
#                     else:
#                         hLim1 = 0
#                         hLim2 = 3.14
#                         hLim1_eta = minEta[etaRegion]
#                         hLim2_eta = maxEta[etaRegion]
#                     h = ROOT.TH1F(histNameAlias+dim+"_layer%d_reg%d"%(lay,etaRegion),
#                                 "PHI in %.3f < %s < %.3f - %s"%(hLim1_eta,"eta",hLim2_eta,layersList[lay]),
#                                 hBins,
#                                 hLim1,
#                                 hLim2)
#                     # leg = ROOT.TLegend(0.7 ,0.6 ,0.85 ,0.75)
#                     # leg.AddEntry(h,"D"+dim+" = %.4f"%(dphi[etaRegion])+" Layer %d"%(lay))
#                     h.GetXaxis().SetTitle('\phi')
#                     h.GetYaxis().SetTitle("Number of events")
#                     h.SetMinimum(0)
#                 else:
#                     hLim1 = -hLim
#                     hLim2 = hLim
#                     h = ROOT.TH1F(histNameAlias+dim+"_layer%d_reg%d"%(lay,etaRegion),
#                                     "DPHI in %.3f < %s < %.3f - %s"%(hLim1,"phi",hLim2,layersList[lay]),
#                                     hBins, 
#                                     hLim1, 
#                                     hLim2)
#                     # leg = ROOT.TLegend(0.7 ,0.6 ,0.85 ,0.75)
#                     # leg.AddEntry(h,"D"+dim+" = %.4f"%(dphi[etaRegion])+" Layer %d"%(lay))
#                     h.GetXaxis().SetTitle('\phi')
#                     h.GetYaxis().SetTitle("Number of events")
#                     h.SetMinimum(0)
#             h.SetDirectory(0) #dont add to the internal registry
#             layList.append( h )
#         # print(np.shape(layList), layList)
#         hList.append( layList )
#         # print(np.shape(hList),hList[lay])
#         # hList[lay] = layList
        
#     return hList 


def histLayerDynBinDeltaPhi(dictCalo, dim='phi', hLim=0.3, useRegionLimits=False, negSide=False, divBinEta=1, divBinPhi=1, useDelta=False):
    hList = []
    # hList = np.empty(np.shape(dictCalo['Layer'])[0], dtype=object)
    # hList[...] = [[] for _ in range(hList.shape[0])]
    layersList = dictCalo['Layer']
    # if dim == 'eta':
    RegionMinList = dictCalo['etaLowLim']
    RegionMaxList = dictCalo['etaHighLim']
    detaList = dictCalo['granularityEtaLayer']
    dphiList = dictCalo['granularityPhiLayer']

    for lay in range(0, len(layersList)):
        layList = []
        maxEta = np.array(RegionMaxList[lay])
        minEta = np.array(RegionMinList[lay])
        # deta   = np.array(     detaList[lay])
        dphi   = np.array(     dphiList[lay])
        # print(deta)

        for phiRegion in range(0, np.size(minEta)):
            # print( "%.3f < %s < %.3f - %s"%(minEta[phiRegion],dim,maxEta[phiRegion],layersList[lay]))
            if dim=='phi':
                regionSize = 3.1415
                hBins =  int(round(regionSize/dphi[phiRegion] / divBinPhi ))
                # hBins = int(round(nCellsHalfRegion))
                hLim1_phi = -3.14
                hLim2_phi = 3.14
                hLim1 = 0
                hLim2 = 3.14
                h = ROOT.TH1F("D"+dim+" = %.4f"%(dphi[phiRegion])+" Layer %d"%(lay),
                            "DPHI in %.3f < %s < %.3f - %s"%(hLim1_phi,"phi",hLim2_phi,layersList[lay]),
                            hBins, 
                            hLim1, 
                            hLim2)
                hLim1 = -hLim
                hLim2 = hLim
                h = ROOT.TH1F("D"+dim+" = %.4f"%(dphi[phiRegion])+" Layer %d"%(lay),
                                "DPHI in %.3f < %s < %.3f - %s"%(hLim1,"phi",hLim2,layersList[lay]),
                                hBins, 
                                hLim1, 
                                hLim2)
            h.SetDirectory(0) #dont add to the internal registry
            layList.append( h )
        # print(np.shape(layList), layList)
        hList.append( layList )
        # print(np.shape(hList),hList[lay])
        # hList[lay] = layList
        
    return hList 



# def histLayerRegionDynBin(dictCalo, dim='eta', name='test', hLim=0.3 ):
#     hList = []
#     # hList = np.empty(np.shape(dictCalo['Layer'])[0], dtype=object)
#     # hList[...] = [[] for _ in range(hList.shape[0])]
#     layersList = dictCalo['Layer']
#     # if dim == 'eta':
#     RegionMinList = dictCalo['etaLowLim']
#     RegionMaxList = dictCalo['etaHighLim']
#     detaList = dictCalo['granularityEtaLayer']
#     dphiList = dictCalo['granularityPhiLayer']

#     for lay in range(0, len(layersList)):
#         layList = []
#         maxEta = np.array(RegionMaxList[lay])
#         minEta = np.array(RegionMinList[lay])
#         deta   = np.array(     detaList[lay])
#         dphi   = np.array(     dphiList[lay])
#         # print(deta)

#         for etaRegion in range(0, np.size(minEta)):
#             # print( "%.3f < %s < %.3f - %s"%(minEta[etaRegion],dim,maxEta[etaRegion],layersList[lay]))
            
#             # Calculating the bin number
#             if dim=='eta':
#                 regionSize = (maxEta[etaRegion] - minEta[etaRegion])
#                 hBins =  int(round(regionSize/deta[etaRegion] / 1 ))
#                 # hBins = int(round(nCellsHalfRegion))
#                 if hBins < 10:
#                     hBins = 10
#                 h = ROOT.TH1F("D"+dim+" = %.4f"%(deta[etaRegion])+" Layer %d"%(lay),
#                             "%s - %.3f < %s < %.3f - %s"%(name, minEta[etaRegion],dim,maxEta[etaRegion],layersList[lay]),
#                             hBins, 
#                             -hLim, 
#                             hLim)
#             if dim=='phi':
#                 regionSize = 2*3.1415
#                 hBins =  int(round(regionSize/dphi[etaRegion] / 1 ))
#                 # hBins = int(round(nCellsHalfRegion))
#                 if hBins < 8:
#                     hBins = 8
#                 h = ROOT.TH1F("D"+dim+" = %.4f"%(dphi[etaRegion])+" Layer %d"%(lay),
#                                 "%s - %.3f < %s < %.3f - %s"%(name, minEta[etaRegion],dim,maxEta[etaRegion],layersList[lay]),
#                                 hBins, 
#                                 -hLim, 
#                                 hLim)
#             h.SetDirectory(0) #dont add to the internal registry
#             layList.append( h )
#         # print(np.shape(layList), layList)
#         hList.append( layList )
#         # print(np.shape(hList),hList[lay])
#         # hList[lay] = layList
        
#     return hList   
