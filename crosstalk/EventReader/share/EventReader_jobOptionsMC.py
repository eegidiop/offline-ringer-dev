###############################################################
#
# Job options file
#
#==============================================================

#--------------------------------------------------------------
# ATLAS default Application Configuration options
#--------------------------------------------------------------
import AthenaCommon.AtlasUnixStandardJob

#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
isMC = True
# doMC = True
# dumpCells = False
# doPhoton = False
from AthenaCommon.AppMgr import ToolSvc
from AthenaCommon.AthenaCommonFlags import jobproperties as jps

# Full job is a list of algorithms
from AthenaCommon.AlgSequence import AlgSequence
job = AlgSequence()

from EventReader.EventReaderConf import EventReaderAlg

#### Geometry relation flags
from AthenaCommon.GlobalFlags import jobproperties
from AthenaCommon.DetFlags import DetFlags
from AthenaCommon.GlobalFlags import globalflags

Geometry = "ATLAS-R2-2016-01-00-01"
globalflags.DetGeo.set_Value_and_Lock('atlas')
#### ! If data is MC, comment the line below! #####
if not(isMC):
    globalflags.DataSource.set_Value_and_Lock('data') #
###################################################
DetFlags.detdescr.all_setOn()
DetFlags.Forward_setOff()
DetFlags.ID_setOff()

jobproperties.Global.DetDescrVersion = Geometry

    # We need the following two here to properly have
    # Geometry
from AtlasGeoModel import GeoModelInit
from AtlasGeoModel import SetGeometryVersion
include("CaloDetMgrDetDescrCnv/CaloDetMgrDetDescrCnv_joboptions.py")
include("LArDetDescr/LArDetDescr_joboptions.py")
#####

# Cabling map acess (LAr)
from LArCabling.LArCablingAccess import LArOnOffIdMapping
LArOnOffIdMapping()

job += EventReaderAlg( "EventReader" )

job.EventReader.clusterName                 = "CaloCalTopoClusters"#"egamma711Clusters" # "CaloCalTopoClusters"
job.EventReader.jetName                     = "AntiKt4EMPFlowJets"
job.EventReader.roiRadius                   = 0.2
job.EventReader.tileDigName                 = "TileDigitsCnt"
if isMC:        
    job.EventReader.larDigName              = "LArDigitContainer_MC"
else:           
    job.EventReader.larDigName              = 'FREE'
job.EventReader.tileRawName                 = "TileRawChannelCnt"
job.EventReader.larRawName                  = "LArRawChannels"
job.EventReader.doClusterDump               = False # Dump only a cluster container.
# job.EventReader.doTile                      = True
job.EventReader.noBadCells                  = True
# job.EventReader.doLAr                       = True
job.EventReader.printCellsClus              = False
job.EventReader.printCellsJet               = False
job.EventReader.testCode                    = False
## Electrons    
job.EventReader.doTagAndProbe               = True  #select by tag and probe method, electron pairs
job.EventReader.doElecSelectByTrackOnly     = False  #select only single electrons which pass track criteria
job.EventReader.getAssociatedTopoCluster    = True #Get the topo cluster associated to a super cluster, which was linked to an Electron
##  
job.EventReader.OutputLevel                 = INFO #DEBUG # 
job.EventReader.isMC                        = isMC  # set to True in case of MC sample.

# if job.EventReader.isMC==False:
#     globalflags.DataSource.set_Value_and_Lock('data') 


from AthenaCommon.AppMgr import ServiceMgr
import AthenaPoolCnvSvc.ReadAthenaPool

# testFile = "/eos/user/m/mhufnage/scripts_lxplus/Reco/ALP_reco/reco_HITStoESD/ESD_pi0.pool.root"

# testFile = "/eos/user/m/mhufnage/scripts_lxplus/Reco/ALP_reco/reco_RDO_LarDigits/ESD_fromRDO-HITS_PI0_001783.pool.root"
# testFile = "/eos/user/m/mhufnage/scripts_lxplus/Reco/ALP_reco/reco_HITStoESD_AMItags/ESD_pi0.pool.root" #AMI TAG reco
# testFile = "/eos/user/m/mhufnage/scripts_lxplus/Reco/ALP_reco/reco_HITStoESD_AMIdumpCells/ESD_pi0.pool.root"
# inputFile = "/eos/user/m/mhufnage/scripts_lxplus/Reco/ALP_reco/reco_HITStoESD_moreEvents/ESD_pi0.pool.root"

# inputFile = '/eos/user/m/mhufnage/scripts_lxplus/Reco/ALP_reco/reco_HITStoESD_evenMoreEvents/ESD_pi0_002450.pool.root'
# inputFile = '/eos/user/m/mhufnage/scripts_lxplus/Reco/ALP_reco/reco_HITStoESD_evenMoreEvents/ESD_pi0_000001.pool.root'#MC

# inputFile   = '/eos/user/m/mhufnage/scripts_lxplus/Reco/ALP_reco/lowPileUpRun_RAWtoESD/ESD_minBias_1.pool.root' #DATA
# ServiceMgr.EventSelector.InputCollections = [ inputFile ]
inputDataPath   = '/eos/user/m/mhufnage/scripts_lxplus/Reco/ALP_reco/MC_Zee_HITStoESD/'
inputFile   = [inputDataPath+'ESD_Zee_0.pool.root',
               inputDataPath+'ESD_Zee_1.pool.root', #DATA
               inputDataPath+'ESD_Zee_2.pool.root',
               inputDataPath+'ESD_Zee_3.pool.root',
               inputDataPath+'ESD_Zee_4.pool.root',
               inputDataPath+'ESD_Zee_5.pool.root',
               inputDataPath+'ESD_Zee_6.pool.root',
               inputDataPath+'ESD_Zee_7.pool.root',
               inputDataPath+'ESD_Zee_8.pool.root',
               inputDataPath+'ESD_Zee_9.pool.root',
               inputDataPath+'ESD_Zee_10.pool.root',
               inputDataPath+'ESD_Zee_11.pool.root',
               inputDataPath+'ESD_Zee_12.pool.root',
               inputDataPath+'ESD_Zee_13.pool.root',
               inputDataPath+'ESD_Zee_14.pool.root',
               inputDataPath+'ESD_Zee_15.pool.root',
               inputDataPath+'ESD_Zee_16.pool.root',
               inputDataPath+'ESD_Zee_17.pool.root',
               inputDataPath+'ESD_Zee_18.pool.root',
               inputDataPath+'ESD_Zee_19.pool.root' ] 
ServiceMgr.EventSelector.InputCollections = inputFile
ServiceMgr += CfgMgr.THistSvc()

# Create output file
hsvc = ServiceMgr.THistSvc
# jps.AthenaCommonFlags.HistOutputs = ["MYSTREAM:myfile.root"]
# hsvc.Output += [ "rec DATAFILE='run329542_minBias/ESD_1_dumped.root' OPT='RECREATE'" ]
# hsvc.Output += [ "rec DATAFILE='ntuples/test_MCZee.root' OPT='RECREATE'" ]
hsvc.Output += [ "rec DATAFILE='ntuples/test_clusSize_MCZee_topoClus.root' OPT='RECREATE'" ]
theApp.EvtMax = 500

MessageSvc.defaultLimit = 9999999  # all messages