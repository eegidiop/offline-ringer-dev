import os
from glob import glob

# ********************************************************************
#  - Local usage -
# File to execute the EventReader dumper into a loop over multiple 
# reconstructed files. It will print directly on bash the commands
# for executing each file, using the 'EventReader_jobOptions_parse.py'
# file.
#*********************************************************************

# os.system('echo checkFile.py myHITS.pool.root.1;checkFile.py myHITS.pool.root.1 > contentsHITS.txt')

hitsFileDir     = '/eos/user/m/mhufnage/scripts_lxplus/Reco/ALP_reco/reco_HITStoESD_evenMoreEvents/'
esdFileNames    = glob(hitsFileDir+'ESD_pi0_*.pool.root')
# outputFileName  = esdFileNames
# print(hitsFileNames)
nFiles = range(len(esdFileNames))

os.system('shopt -s expand_aliases; source $HOME/.bashrc; setupATLAS; asetup Athena,latest,master') #asetup Athena,22.0.44'

for f in esdFileNames:
    # fout = f[-16:]    
    fout      = f.split("/")[-1]
    dataAlias = 'ntuples/%s'%(fout)
    command   = 'athena.py -c "FileName=\'%s\'; OutputFileName=\'%s\';" EventReader_jobOptions_parse.py '%(f, dataAlias)

    os.system(+command)
    os.system(command)
