import ROOT
import numpy as np
# from ROOT import TH1F, TTree, TChain, TFile, gROOT
from glob import glob
from time import time
from histHelper import histLayerRegionFixBin, histLayerDynBin, histTH1F, histTH1D, histTH2F, plotTH1F, plotTH2F  #custom
from functionsHelper import correctedDeltaPhi, calcDeltaEta, getCellEneTimeFromChannel, listToNumpyArray, digitsToList, isInsideLArCrackRegion, loadJsonFile, saveAsJsonFile, getCentralCellIndexJetROI_deltaR #custom
import math
import argparse
import logging
import os

ROOT.gROOT.SetBatch(True)
# ROOT.gROOT.SetStyle("ATLAS")
################################

############################
#### PARSE ARGUMENTS   #####
############################
# Usage example: 
#   python validPlots.py  --getNoise=True
#   python validPlots.py --inputFilesPath='/eos/user/m/mhufnage/scripts_lxplus/Grid/workAthena/ALPPackages/run/ntuples/ESD_pi0_*.root' --outputFilesPath='/eos/user/m/mhufnage/scripts_lxplus/Grid/workAthena/ALPPackages/run/singlePi0_50kEvts'

########################################
#######  PATHS AND FILENAMES    ########
########################################
outputFilesPath = '/eos/user/m/mhufnage/scripts_lxplus/Grid/workAthena/ALPPackages/run/testRepeatedCells/' #path for the 'run' dir, where the package will be executed

# inputFilesPath  = '/eos/user/m/mhufnage/scripts_lxplus/Grid/workAthena/ALPPackages/run/ntuples/test_minbias_repeatedCells.root' #dumped files absolute path
# dataType    = 'data'
inputFilesPath  = '/eos/user/m/mhufnage/scripts_lxplus/Grid/workAthena/ALPPackages/run/ntuples/test_MCZee_repeatedCells.root'
dataType    = 'MC' 

# outputFolder    = 'singlePi0' #name of output folder for dumped data pointed in 'inputFilesPath'

# inputFileName  = glob(cernbox+'scripts_lxplus/Grid/workAthena/ALPPackages/run/ntuples/ntuple_singlePi0_Test.root') #lxplus
# outputDir = cernbox+'scripts_lxplus/Grid/workAthena/ALPPackages/run/singlePi0_test/' #lxplus
inputFileName   = glob('{}'.format(inputFilesPath)) # ntuple_singlePi0_Truth000001
# outputDir       = '{}/'.format(outputFilesPath) #lxplus
fDictLayerName  = 'dictCaloByLayer.json'
# inputFileName = '/home/mhufnagel/cernbox/scripts_lxplus/Grid/workAthena/ALPPackages/run/ntuple.root' #local

############################
##   LOAD DUMPED DATA     ##
############################

sTree = ROOT.TChain("dumpedData",'')
for file in inputFileName:
    sTree.Add(file+"/dumpedData")


##################################################################################################################
####### Configuration ######
############################

# ****** DICTIONARIES ******
dictCaloLayer       = loadJsonFile(fDictLayerName) # Load Calo Geometry Dictionary
transitionEtaWidthL = dictCaloLayer["transitionRegionEta"] #0.2 # eta value around the transition regions to consider (or discard) cells which could be tagged as belonging to another region in the same layer.
transitionEtaWidthH = dictCaloLayer["transitionRegionEta"] #0.2 # (eta + transitionEtaWidthL) < Eta < (eta - transitionEtaWidthH). A value for each layer.
maxOfLayers         = len(dictCaloLayer['Layer']) 

# Histograms (Cluster - Cell)
hClusterCellEta = histTH1D('clus_eta_cell', 'clus_eta_cell',60,-3,3)
hClusterCellPhi = histTH1D('clus_phi_cell', 'clus_phi_cell',64,-(math.pi + 0.1),(math.pi + 0.1))
hClusterChannelEnergy = histTH1D('clus_ch_ene','clus_ch_ene',200, -10, 80)

hClusterCellEtaRep = histTH1D('clus_eta_cell_rep', 'clus_eta_cell_rep',60,-3,3)
hClusterCellPhiRep = histTH1D('clus_phi_cell_rep', 'clus_phi_cell_rep',64,-(math.pi + 0.1),(math.pi + 0.1))
hClusterCellEnergyRep = histTH1D('clus_cell_ene_rep','clus_cell_ene_rep',200, -10, 80)

hEtaPhiMap          = histTH2F('clus_eta_phi_map_rep','clus_eta_phi_map_rep',20,-3,3,20,-(math.pi + 0.1),(math.pi + 0.1))
hEtaPhiMapRep       = histTH2F('clus_eta_phi_map_rep','clus_eta_phi_map_rep',20,-3,3,20,-(math.pi + 0.1),(math.pi + 0.1))

# Number of regions in each sampling layer (eta x phi region)
regionsPerLayer = []
for reg in dictCaloLayer['granularityEtaLayer']:
    regionsPerLayer.append(len(reg))

# evN = 0
evLim = sTree.GetEntries()#50000

# Histogram files
# layIndex = 1
start = time()
for evN in range(0,evLim):
    sTree.GetEntry(evN)
    if (evN % 500) == 0:
        evtLogInfo = "Event %d / %d"%(evN, evLim)
        print(evtLogInfo)
        logging.info(evtLogInfo)

    # if loadClusters or xTalkStudiesValid:
    # clusterIndex                 = getattr(sTree,'cluster_index')
    # clusterPt                    = getattr(sTree,'cluster_pt')
    # clusterEta                   = getattr(sTree,'cluster_eta')
    # clusterPhi                   = getattr(sTree,'cluster_phi')
    # clusterEtaCalc               = getattr(sTree,'cluster_eta_calc')
    # clusterPhiCalc               = getattr(sTree,'cluster_phi_calc')
    # Cluster Cells 
    clusterCellIndex             = getattr(sTree,'cluster_cell_index') 
    clusterIndexCellLvl          = getattr(sTree,'cluster_index_cellLvl')
    clusterCellCaloGain          = getattr(sTree,'cluster_cell_caloGain')
    clusterCellLayer             = getattr(sTree,'cluster_cell_layer')
    clusterCellRegion            = getattr(sTree,'cluster_cell_region')
    clusterCellEta               = getattr(sTree,'cluster_cell_eta')
    clusterCellPhi               = getattr(sTree,'cluster_cell_phi') 

    clusterCellEtaRep               = getattr(sTree,'cluster_cell_eta_repeated')
    clusterCellPhiRep               = getattr(sTree,'cluster_cell_phi_repeated') 
    clusterCellEnergyRep       = getattr(sTree,'cluster_cell_energy_repeated')

    # clusterCellDEta              = getattr(sTree,'cluster_cell_deta')
    # clusterCellDPhi              = getattr(sTree,'cluster_cell_dphi')
    # clusterCellsDistDEta         = getattr(sTree,'cluster_cellsDist_deta')
    # clusterCellsDistDPhi         = getattr(sTree,'cluster_cellsDist_dphi')
    # Cluster Channel 
    clusterIndexChLvl            = getattr(sTree,'cluster_index_chLvl')
    clusterChannelIndex          = getattr(sTree,'cluster_channel_index')
    # clusterChannelDigits         = getattr(sTree,'cluster_channel_digits')
    clusterChannelEnergy         = getattr(sTree,'cluster_channel_energy')
    # clusterChannelTime           = getattr(sTree,'cluster_channel_time')
    # if dataHasBadCh: clusterChannelBad            = getattr(sTree,'cluster_channel_bad')
    clusterChannelChInfo         = getattr(sTree,'cluster_channel_chInfo')
    # Cluster raw channel 
    clusterIndexRawChLvl         = getattr(sTree,'cluster_index_rawChLvl')
    clusterRawChannelIndex       = getattr(sTree,'cluster_rawChannel_index')
    clusterRawChannelAmplitude   = getattr(sTree,'cluster_rawChannel_amplitude')
    # clusterRawChannelTime        = getattr(sTree,'cluster_rawChannel_time')
    # clusterRawChannelPedProv     = getattr(sTree,'cluster_rawChannel_pedProv')
    # clusterRawChannelQual        = getattr(sTree,'cluster_rawChannel_qual')
    # clusterRawChannelChInfo      = getattr(sTree,'cluster_rawChannel_chInfo')

    #**********************************************
    # Format into python list/np array
    #**********************************************
   
    # clusterIndexArray               = listToNumpyArray(clusterIndex)
    # clusterPtArray                  = listToNumpyArray(clusterPt)
    # clusterEtaArray                 = listToNumpyArray(clusterEta)
    # clusterPhiArray                 = listToNumpyArray(clusterPhi)
    # clusterEtaCalcArray             = listToNumpyArray(clusterEtaCalc)
    # clusterPhiCalcArray             = listToNumpyArray(clusterPhiCalc)

    clusterCellIndexArray           = listToNumpyArray(clusterCellIndex)
    clusterIndexCellLvlArray        = listToNumpyArray(clusterIndexCellLvl)
    clusterCellCaloGainArray        = listToNumpyArray(clusterCellCaloGain)
    clusterCellLayerArray           = listToNumpyArray(clusterCellLayer)
    clusterCellRegionArray          = listToNumpyArray(clusterCellRegion)
    clusterCellEtaArray             = listToNumpyArray(clusterCellEta)
    clusterCellPhiArray             = listToNumpyArray(clusterCellPhi)

    clusterCellEtaRepArray          = listToNumpyArray(clusterCellEtaRep)
    clusterCellPhiRepArray          = listToNumpyArray(clusterCellPhiRep)
    clusterCellEnergyRepArray       = listToNumpyArray(clusterCellEnergyRep)
    # clusterCellDEtaArray            = listToNumpyArray(clusterCellDEta)
    # clusterCellDPhiArray            = listToNumpyArray(clusterCellDPhi)
    # clusterCellsDistDEtaArray       = listToNumpyArray(clusterCellsDistDEta)
    # clusterCellsDistDPhiArray       = listToNumpyArray(clusterCellsDistDPhi)
    
    clusterIndexChLvlArray          = listToNumpyArray(clusterIndexChLvl)
    clusterChannelIndexArray        = listToNumpyArray(clusterChannelIndex)
    # clusterChannelDigitsArray       = digitsToList(clusterChannelDigits)
    clusterChannelEnergyArray       = listToNumpyArray(clusterChannelEnergy)
    # clusterChannelTimeArray         = listToNumpyArray(clusterChannelTime)
    # if dataHasBadCh: clusterChannelBadArray      = listToNumpyArray(clusterChannelBad)

    clusterIndexRawChLvlArray       = listToNumpyArray(clusterIndexRawChLvl)
    clusterRawChannelIndexArray     = listToNumpyArray(clusterRawChannelIndex)
    clusterRawChannelAmplitudeArray = listToNumpyArray(clusterRawChannelAmplitude)
    # clusterRawChannelTimeArray      = listToNumpyArray(clusterRawChannelTime)
    # clusterRawChannelPedProvArray   = listToNumpyArray(clusterRawChannelPedProv)
    # clusterRawChannelQualArray      = listToNumpyArray(clusterRawChannelQual)
    # clusterRawChannelChInfoArray    = listToNumpyArray(clusterRawChannelChInfo)

    ###################################################################        
    # ***************
    #   HISTOGRAMS
    # ***************
    for cellIndex in range(0, len(clusterCellEtaArray)):
        hClusterCellEta.Fill(clusterCellEtaArray[cellIndex])
        hClusterCellPhi.Fill(clusterCellPhiArray[cellIndex])
        hEtaPhiMap.Fill(clusterCellEtaArray[cellIndex], clusterCellPhiArray[cellIndex])
    
    for chIndex in range(0, len(clusterChannelIndexArray)):
        hClusterChannelEnergy.Fill(clusterChannelEnergyArray[chIndex]/1000)
        
    for cellIndex in range(0, len(clusterCellEtaRepArray)):
        hClusterCellEtaRep.Fill(clusterCellEtaRepArray[cellIndex])
        hClusterCellPhiRep.Fill(clusterCellPhiRepArray[cellIndex])
        hEtaPhiMapRep.Fill(clusterCellEtaRepArray[cellIndex], clusterCellPhiRepArray[cellIndex])
        hClusterCellEnergyRep.Fill(clusterCellEnergyRep[cellIndex]/1000)


### PLOT HISTOGRAMS
# hClusterCellEta = histTH1D('clus_eta_cell', 'clus_eta_cell',100,-6,6)
# hClusterCellPhi = histTH1D('clus_phi_cell', 'clus_phi_cell',64,-math.pi,math.pi)
# hClusterChannelEnergy = histTH1D('clus_ch_ene','clus_ch_ene',200, -10, 200)

# hClusterCellEtaRep = histTH1D('clus_eta_cell_rep', 'clus_eta_cell_rep',100,-6,6)
# hClusterCellPhiRep = histTH1D('clus_phi_cell_rep', 'clus_phi_cell_rep',64,-math.pi,math.pi)
# hClusterCellEnergyRep = histTH1D('clus_cell_ene_rep','clus_cell_ene_rep',200, -10, 200)

# hEtaPhiMap          = histTH2F('clus_eta_phi_map_rep','clus_eta_phi_map_rep',20,-6,6,20,-math.pi,math.pi)
# hEtaPhiMapRep       = histTH2F('clus_eta_phi_map_rep','clus_eta_phi_map_rep',20,-6,6,20,-math.pi,math.pi)
textPos     = [0.6, 0.12]
# ---------------
#   ENERGY
# ---------------
c           = ROOT.TCanvas()
ROOT.gPad.SetLogy(True) # actual subpad
hClusterChannelEnergy.GetYaxis().SetTitleOffset(1.35)
hClusterChannelEnergy.GetYaxis().SetTitle("Events")
hClusterChannelEnergy.GetXaxis().SetTitle("Energy [GeV]")
hClusterChannelEnergy.SetTitle("Cells in calo cluster versus repeated cells.")
hClusterChannelEnergy.Draw("hist e")
hClusterCellEnergyRep_ratio = hClusterCellEnergyRep.Clone()
hClusterCellEnergyRep_ratio.Scale(hClusterChannelEnergy.Integral() / hClusterCellEnergyRep_ratio.Integral())
hClusterCellEnergyRep_ratio.Draw("same e")
hClusterCellEnergyRep_ratio.SetLineColor(ROOT.kRed)

# legend      = ROOT.TLegend(textPos[0] ,textPos[1] ,textPos[0] + 0.2 ,textPos[1]+0.1)
legend      = c.BuildLegend(textPos[0], textPos[1], textPos[0] + 0.2 ,textPos[1] + 0.1)
legend.Clear()
# legend.SetTextSize(0.025)
legend.SetLineWidth(0)
legText     = "Cells without repetition ({}k entries)".format(int(hClusterChannelEnergy.GetEntries()/1000))
legTextCut  = "Repeated cells ({}k entries)".format(int(hClusterCellEnergyRep_ratio.GetEntries()/1000))
legend.AddEntry(hClusterChannelEnergy, legText)
legend.AddEntry(hClusterCellEnergyRep_ratio, legTextCut)
legend.Draw()
# pad.BuildLegend(textPos[0] ,textPos[1] ,textPos[0] + 0.2 ,textPos[1]+0.1)

latex = ROOT.TLatex()
latex.SetNDC()
latex.SetTextSize(0.040)
latex.DrawText(textPos[0] + 0.01 ,textPos[1] + 0.17 ,"EventReader Dumper")
latex.SetTextSize(0.030)
if dataType=='MC':
    latex.DrawLatex(textPos[0] + 0.01 ,textPos[1] + 0.12 ,"MC Zee single e^{-}" )
if dataType=='data': 
    latex.DrawLatex(textPos[0] + 0.01 ,textPos[1] + 0.12 ,"Data single e^{-}" )
latex.SetTextSize(0.020)
latex.DrawLatex(textPos[0] + 0.01 ,textPos[1] + 0.10 ,"ratio of repetition #approx %.2f %%"%(hClusterCellEnergyRep_ratio.GetEntries()/hClusterChannelEnergy.GetEntries() * 100) )

c.Print(outputFilesPath+dataType+"_energyRepeatedComparison.pdf")
# c.Print(dirPlots+"%s.pdf]"%(plType))
c.Clear()


# ---------------
#   ETA
# ---------------
c           = ROOT.TCanvas()
hClusterCellEta.GetYaxis().SetTitleOffset(1.35)
hClusterCellEta.GetYaxis().SetTitle("Events")
hClusterCellEta.GetXaxis().SetTitle("Eta")
hClusterCellEta.SetTitle("Cells in calo cluster versus repeated cells.")
hClusterCellEta.Draw("hist e")
hClusterCellEtaRep_ratio = hClusterCellEtaRep.Clone()
hClusterCellEtaRep_ratio.Scale(hClusterCellEta.Integral() / hClusterCellEtaRep_ratio.Integral())
hClusterCellEtaRep_ratio.Draw("same e")
hClusterCellEtaRep_ratio.SetLineColor(ROOT.kRed)

# legend      = ROOT.TLegend(textPos[0] ,textPos[1] ,textPos[0] + 0.2 ,textPos[1]+0.1)
legend      = c.BuildLegend(textPos[0], textPos[1], textPos[0] + 0.2 ,textPos[1] + 0.1)
legend.Clear()
# legend.SetTextSize(0.025)
legend.SetLineWidth(0)
legText     = "Cells without repetition ({}k entries)".format(int(hClusterCellEta.GetEntries()/1000))
legTextCut  = "Repeated cells ({}k entries)".format(int(hClusterCellEtaRep_ratio.GetEntries()/1000))
legend.AddEntry(hClusterCellEta, legText)
legend.AddEntry(hClusterCellEtaRep_ratio, legTextCut)
legend.Draw()
# pad.BuildLegend(textPos[0] ,textPos[1] ,textPos[0] + 0.2 ,textPos[1]+0.1)

latex = ROOT.TLatex()
latex.SetNDC()
latex.SetTextSize(0.040)
latex.DrawText(textPos[0] + 0.01 ,textPos[1] + 0.17 ,"EventReader Dumper")
latex.SetTextSize(0.030)
if dataType=='MC':
    latex.DrawLatex(textPos[0] + 0.01 ,textPos[1] + 0.12 ,"MC Zee single e^{-}" )
if dataType=='data': 
    latex.DrawLatex(textPos[0] + 0.01 ,textPos[1] + 0.12 ,"Data single e^{-}" )
latex.SetTextSize(0.020)
latex.DrawLatex(textPos[0] + 0.01 ,textPos[1] + 0.10 ,"ratio of repetition #approx %.2f %%"%(hClusterCellEtaRep_ratio.GetEntries()/hClusterCellEta.GetEntries() * 100) )

c.Print(outputFilesPath+dataType+"_etaRepeatedComparison.pdf")
# c.Print(dirPlots+"%s.pdf]"%(plType))
c.Clear()

# ---------------
#   PHI
# ---------------
c           = ROOT.TCanvas()
hClusterCellPhi.GetYaxis().SetTitleOffset(1.35)
hClusterCellPhi.GetYaxis().SetTitle("Events")
hClusterCellPhi.GetXaxis().SetTitle("Phi")
hClusterCellPhi.SetTitle("Cells in calo cluster versus repeated cells.")
hClusterCellPhi.Draw("hist e")
hClusterCellPhiRep_ratio = hClusterCellPhiRep.Clone()
hClusterCellPhiRep_ratio.Scale(hClusterCellPhi.Integral() / hClusterCellPhiRep_ratio.Integral())
hClusterCellPhiRep_ratio.Draw("same e")
hClusterCellPhiRep_ratio.SetLineColor(ROOT.kRed)

# legend      = ROOT.TLegend(textPos[0] ,textPos[1] ,textPos[0] + 0.2 ,textPos[1]+0.1)
legend      = c.BuildLegend(textPos[0], textPos[1], textPos[0] + 0.2 ,textPos[1] + 0.1)
legend.Clear()
# legend.SetTextSize(0.025)
legend.SetLineWidth(0)
legText     = "Cells without repetition ({}k entries)".format(int(hClusterCellPhi.GetEntries()/1000))
legTextCut  = "Repeated cells ({}k entries)".format(int(hClusterCellPhiRep_ratio.GetEntries()/1000))
legend.AddEntry(hClusterCellPhi, legText)
legend.AddEntry(hClusterCellPhiRep_ratio, legTextCut)
legend.Draw()
# pad.BuildLegend(textPos[0] ,textPos[1] ,textPos[0] + 0.2 ,textPos[1]+0.1)

latex = ROOT.TLatex()
latex.SetNDC()
latex.SetTextSize(0.040)
latex.DrawText(textPos[0] + 0.01 ,textPos[1] + 0.17 ,"EventReader Dumper")
latex.SetTextSize(0.030)
if dataType=='MC':
    latex.DrawLatex(textPos[0] + 0.01 ,textPos[1] + 0.12 ,"MC Zee single e^{-}" )
if dataType=='data': 
    latex.DrawLatex(textPos[0] + 0.01 ,textPos[1] + 0.12 ,"Data single e^{-}" )
latex.SetTextSize(0.020)
latex.DrawLatex(textPos[0] + 0.01 ,textPos[1] + 0.10 ,"ratio of repetition #approx %.2f %%"%(hClusterCellPhiRep_ratio.GetEntries()/hClusterCellPhi.GetEntries() * 100) )

c.Print(outputFilesPath+dataType+"_phiRepeatedComparison.pdf")
# c.Print(dirPlots+"%s.pdf]"%(plType))
c.Clear()

# ------------------------------
#   ETA - PHI (non-repeated)
# ------------------------------

c           = ROOT.TCanvas()
ROOT.gPad.SetLogz(True) # actual subpad
hEtaPhiMap.GetYaxis().SetTitleOffset(1.35)
hEtaPhiMap.GetZaxis().SetTitle("Events")
hEtaPhiMap.GetYaxis().SetTitle("Phi")
hEtaPhiMap.GetXaxis().SetTitle("Eta")
hEtaPhiMap.SetTitle("Cells in calo cluster versus repeated cells eta x phi map.")

hEtaPhiMap.Draw('colz')

# legend      = ROOT.TLegend(textPos[0] ,textPos[1] ,textPos[0] + 0.2 ,textPos[1]+0.1)
# legend      = c.BuildLegend(textPos[0], textPos[1], textPos[0] + 0.2 ,textPos[1] + 0.1)
# legend.Clear()
# legend.SetTextSize(0.025)
# legend.SetLineWidth(0)
# legText     = "Cells without repetition ({}k entries)".format(int(hClusterCellPhi.GetEntries()/1000))
# legTextCut  = "Repeated cells ({}k entries)".format(int(hClusterCellPhiRep_ratio.GetEntries()/1000))
# legend.AddEntry(hClusterCellPhi, legText)
# legend.AddEntry(hClusterCellPhiRep_ratio, legTextCut)
# legend.Draw()
# pad.BuildLegend(textPos[0] ,textPos[1] ,textPos[0] + 0.2 ,textPos[1]+0.1)

latex = ROOT.TLatex()
latex.SetNDC()
latex.SetTextSize(0.040)
latex.DrawText(textPos[0] + 0.01 ,textPos[1] + 0.17 ,"EventReader Dumper")
latex.SetTextSize(0.030)
if dataType=='MC':
    latex.DrawLatex(textPos[0] + 0.01 ,textPos[1] + 0.12 ,"MC Zee single e^{-}" )
if dataType=='data': 
    latex.DrawLatex(textPos[0] + 0.01 ,textPos[1] + 0.12 ,"Data single e^{-}" )
latex.SetTextSize(0.020)
# latex.DrawLatex(textPos[0] + 0.01 ,textPos[1] + 0.10 ,"ratio of repetition #approx %.2f %%"%(hClusterCellPhiRep_ratio.GetEntries()/hClusterCellPhi.GetEntries() * 100) )

c.Print(outputFilesPath+dataType+"_EtaPhiMap.pdf")
# c.Print(dirPlots+"%s.pdf]"%(plType))
c.Clear()

# ------------------------------
#   ETA - PHI (repeated)
# ------------------------------

c           = ROOT.TCanvas()
ROOT.gPad.SetLogz(True) # actual subpad
hEtaPhiMapRep.GetYaxis().SetTitleOffset(1.35)
hEtaPhiMapRep.GetZaxis().SetTitle("Events")
hEtaPhiMapRep.GetYaxis().SetTitle("Phi")
hEtaPhiMapRep.GetXaxis().SetTitle("Eta")
hEtaPhiMapRep.SetTitle("Cells in calo cluster versus repeated cells eta x phi map.")

hEtaPhiMapRep.Draw('colz')

# legend      = ROOT.TLegend(textPos[0] ,textPos[1] ,textPos[0] + 0.2 ,textPos[1]+0.1)
# legend      = c.BuildLegend(textPos[0], textPos[1], textPos[0] + 0.2 ,textPos[1] + 0.1)
# legend.Clear()
# legend.SetTextSize(0.025)
# legend.SetLineWidth(0)
# legText     = "Cells without repetition ({}k entries)".format(int(hClusterCellPhi.GetEntries()/1000))
# legTextCut  = "Repeated cells ({}k entries)".format(int(hClusterCellPhiRep_ratio.GetEntries()/1000))
# legend.AddEntry(hClusterCellPhi, legText)
# legend.AddEntry(hClusterCellPhiRep_ratio, legTextCut)
# legend.Draw()
# pad.BuildLegend(textPos[0] ,textPos[1] ,textPos[0] + 0.2 ,textPos[1]+0.1)

latex = ROOT.TLatex()
latex.SetNDC()
latex.SetTextSize(0.040)
latex.DrawText(textPos[0] + 0.01 ,textPos[1] + 0.17 ,"EventReader Dumper")
latex.SetTextSize(0.030)
if dataType=='MC':
    latex.DrawLatex(textPos[0] + 0.01 ,textPos[1] + 0.12 ,"MC Zee single e^{-}" )
if dataType=='data': 
    latex.DrawLatex(textPos[0] + 0.01 ,textPos[1] + 0.12 ,"Data single e^{-}" )
latex.SetTextSize(0.020)
# latex.DrawLatex(textPos[0] + 0.01 ,textPos[1] + 0.10 ,"ratio of repetition #approx %.2f %%"%(hClusterCellPhiRep_ratio.GetEntries()/hClusterCellPhi.GetEntries() * 100) )

c.Print(outputFilesPath+dataType+"_EtaPhiMapRepeated.pdf")
# c.Print(dirPlots+"%s.pdf]"%(plType))
c.Clear()

# histFile = ROOT.TFile(outputDirHist+xTalkFileName, "RECREATE")
# hElectronPt.Write()
# hRatioDigitsRawCh.Write()

# histFile.Close()

end = time()
timeLog = "Total time for %d events: %.3f minutes."%(evLim,(end-start)/60)
print(timeLog)

ROOT.gROOT.SetBatch(False)

# /usr/bin/python3 /eos/user/m/mhufnage/scripts_lxplus/Grid/workAthena/ALPPackages/source/EventReader/share/validPlots.py
# /usr/bin/python3 /eos/user/m/mhufnage/scripts_lxplus/Grid/workAthena/ALPPackages/source/EventReader/share/saveCaloDict.py
# 