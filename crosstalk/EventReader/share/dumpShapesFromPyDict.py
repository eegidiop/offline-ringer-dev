import json
import os,sys
import time

sys.path.insert(1, '/eos/user/m/mhufnage/scripts_lxplus/Grid/workAthena/offline-ringer-dev/crosstalk/EventReader/share')
sys.path.insert(1, '/eos/user/m/mhufnage/SWAN_projects/XTalk/')

from auxiliaryFunctions import *
from calibrationFilesHelper import *

# import ROOT
import numpy as np
from glob import glob
import math
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib.colors import LinearSegmentedColormap
import matplotlib

#--------------------------
#   Configuration (Start)
#--------------------------
# Data
eventsBlockList = [-99999999]

datasetPath     ='/eos/user/m/mhufnage/scripts_lxplus/Grid/workAthena/offline-ringer-dev/crosstalk/run/dataMinBias_Shaper/'
dataset        = datasetPath+'dataMinBias_getShaper_part*.npz'

resultFileAlias     = 'Shapes'

onlyPlot        = True

bWindow             = False # dump the window *(true) or the cluster (false) cells
bFullWindows        = False # use only full windows from clusters (false to increase statistics)
#------------- DSP conditions #-------------
typeThreshold       = 'EneThr'#'NoiseSigma'#
bAbsDSPE1           = True
#--------------------------------------------
el_thrs             = [0] # GeV
nCellsChEta         = 5
nCellsChPhi         = 5
nCellsRawChEta      = 5
nCellsRawChPhi      = 5
clusNames           = ['clusters','711_roi'] #'clusters'
layers              = ["PreSamplerB", "EMB1", "EMB2", "EMB3", "PreSamplerE", "EME1", "EME2", "EME3", "HEC0", "HEC1", "HEC2", "HEC3", "FCAL0", "FCAL1", "FCAL2"]

print("constants loaded succesfully.")

#--------------------------
#  Configuration (End)
#--------------------------

if not(onlyPlot):

    start = time.time()
    evt_lim = 15

    
    fileNames       = glob(dataset)
    if len(fileNames) == 0:
        print("Dataset is empty!\nexiting script...")
        sys.exit()

    for el_thr in el_thrs:
        print('Electron pt cut: {} GeV'.format(el_thr))
        for clusName in clusNames:
            print('Processing cluster: {}'.format(clusName))

            analysis   = [
                        'digits',
                        'digitsSize',
                        'channelEnergy',
                        'channelTime', 
                        'channelInfo',
                        'channelId',
                        'channelGain',
                        'channelEta',
                        'channelPhi',
                        'channelShape',
                        'channelShapeDer'
                        ]

            results              = {key:{key1:[] for key1 in analysis} for key in layers}
            results[clusName]    = {'pt':[],'eta':[],'phi':[]} # Cluster and ROI data

            for file in fileNames:
                print(file)
                dataset     = np.load(file, allow_pickle=True)

                for evtn, ev in enumerate(dataset['dataset']): # for each event...
                    # print("Event number: {}".format(ev['eventNumber']))
                    if (ev['eventNumber'][0] in eventsBlockList):
                        print('Ignoring blocked event {}.'.format(ev['eventNumber']))
                        continue

                    for elec in ev['electrons'].keys(): # for each electron in this event...
                #         print(ev['electrons'][elec]['pt'])
                        if ev['electrons'][elec]['pt'][0]/1000 < el_thr:
                            continue
                        # if np.abs(ev['electrons'][elec]['eta']) > 1.4:
                        #     continue
                        
                        clusloop = ev['electrons'][elec][clusName]

                        for clus in clusloop.keys(): # for each cluster associated to that electron...


                            for layern, layer in enumerate(layers):

                                channelLayerIndexes     = getMatchedIndexes(ev['electrons'][elec][clusName][clus]['channels']['sampling'][0], layer) # get list indexes that match to current
                                rawChannelLayerIndexes  = getMatchedIndexes(ev['electrons'][elec][clusName][clus]['rawChannels']['sampling'][0], layer)

                                # is that layer empty for RAWCh or Ch? skip
                                if (len(channelLayerIndexes) == 0 ) or (len(rawChannelLayerIndexes)==0):
                                    continue

                                ###################     ###############
                                ## Channel + Digits  +  ## RawChannel #
                                ###################     ###############
                                windowIndexesCh, layerIndexesCh = getCellsInWindowCustomSize(
                                    ev['electrons'][elec][clusName][clus]['channels']['energy'],
                                    ev['electrons'][elec][clusName][clus]['channels']['index'],
                                    ev['electrons'][elec][clusName][clus]['channels']['eta'],
                                    ev['electrons'][elec][clusName][clus]['channels']['phi'],
                                    ev['electrons'][elec][clusName][clus]['channels']['sampling'],
                                    nCellsChEta, 
                                    nCellsChPhi, 
                                    ev['electrons'][elec][clusName][clus]['channels']['deta'],
                                    ev['electrons'][elec][clusName][clus]['channels']['dphi'],
                                    layer=layer)
                                
                                # any of the windows aren't full??
                                if bFullWindows:
                                    if (len(windowIndexesCh) != (nCellsChEta*nCellsChPhi)):
                                        continue
                                
                                # if they belong to the same calo object and have the same size, they MUST have the same cells, because the dumper rule is the same.
                                # BUT, they cells aren't stored at the same order.
                                # First, lets check if their cells, in the current layer, are the same!
                                # print("Ok, both windows are full, lets check their cells. RawCh={}, Ch={}".format(len(windowIndexesRawCh),len(windowIndexesCh)))
                                if bWindow:
                                    selectionIndexes = windowIndexesCh
                                else:
                                    selectionIndexes = layerIndexesCh
                                    
                                digits            = np.array(ev['electrons'][elec][clusName][clus]['channels']['digits'][0],dtype=object)       [selectionIndexes] .tolist()
                                digitsSize        = len (np.array(ev['electrons'][elec][clusName][clus]['channels']['digits'][0],dtype=object)  [selectionIndexes] .tolist())
                                channelEnergy     = np.array(ev['electrons'][elec][clusName][clus]['channels']['energy'][0],dtype=object)       [selectionIndexes] .tolist()
                                channelTime       = np.array(ev['electrons'][elec][clusName][clus]['channels']['time'][0],dtype=object)         [selectionIndexes] .tolist()
                                channelInfo       = np.array(ev['electrons'][elec][clusName][clus]['channels']['chInfo'][0],dtype=object)       [selectionIndexes] .tolist()
                                channelId         = np.array(ev['electrons'][elec][clusName][clus]['channels']['channelId'][0],dtype=object)    [selectionIndexes] .tolist()
                                channelSampling   = np.array(ev['electrons'][elec][clusName][clus]['channels']['sampling'][0],dtype=object)     [selectionIndexes] .tolist()
                                channelEta        = np.array(ev['electrons'][elec][clusName][clus]['channels']['eta'][0],dtype=object)          [selectionIndexes] .tolist()
                                channelPhi        = np.array(ev['electrons'][elec][clusName][clus]['channels']['phi'][0],dtype=object)          [selectionIndexes] .tolist()
                                channelGain       = np.array(ev['electrons'][elec][clusName][clus]['channels']['gain'][0],dtype=object)         [selectionIndexes] .tolist()
                                channelShape      = np.array(ev['electrons'][elec][clusName][clus]['channels']['Shape'][0],dtype=object)        [selectionIndexes] .tolist()
                                channelShapeDer   = np.array(ev['electrons'][elec][clusName][clus]['channels']['ShapeDer'][0],dtype=object)     [selectionIndexes] .tolist()

                                # Save variables to posterior analysis
                                results[layer]['digits'].append(digits)
                                results[layer]['digitsSize'].append(digitsSize)
                                results[layer]['channelEnergy'].append(channelEnergy)
                                results[layer]['channelTime'].append(channelTime)
                                results[layer]['channelInfo'].append(channelInfo)
                                results[layer]['channelId'].append(channelId)
                                results[layer]['channelGain'].append(channelGain)
                                results[layer]['channelEta'].append(channelEta)
                                results[layer]['channelPhi'].append(channelPhi)
                                results[layer]['channelShape'].append(channelShape)
                                results[layer]['channelShapeDer'].append(channelShapeDer)
        
        
            print(results.keys())
            
            # np.savez('channelTiming.npz',**timing)
            np.savez(datasetPath+'shapesLAr_analysis{}_{}_etCut{}.npz'.format(resultFileAlias, clusName, el_thr),**results)

            print("files saved in {}".format(datasetPath))
    end = time.time()

    print("Total running time: {:.2f} minutes.".format((end - start)/60))



#----------------------
# Plots
#----------------------

matplotlib.use('Agg') #CHANGES THE MATPLOTLIB BACKEND FOR LOOP PLOTS
# matplotlib.use('module://ipykernel.pylab.backend_inline')  # show plots

clusNames        = ['711_roi']
el_thr          = 0 #GeV
labelFontSize   = 14
xbins           = 30

for clusName in clusNames:
    print(clusName+'...')

    resultFiles = [datasetPath+'shapesLAr_analysis{}_{}_etCut{}.npz'.format(resultFileAlias, clusName, el_thr)]
            
    for resultFile in resultFiles:
        print(resultFile+'...')

        results     = np.load(resultFile, allow_pickle=True)

        for layer in layers:
            print(layer)
            resultsInSampling   = results[layer].tolist()
            if len(resultsInSampling['channelShape']) == 0:
                print('{} is empty! '.format(layer) )
                continue

            digits          = list(resultsInSampling['digits'])
            digitsSize      = list(resultsInSampling['digitsSize'])
            channelEnergy   = list(np.array(np.concatenate(resultsInSampling['channelEnergy']), dtype=object).flat)
            channelTime     = list(np.array(np.concatenate(resultsInSampling['channelTime']), dtype=object).flat)
            channelInfo     = list(np.array(np.concatenate(resultsInSampling['channelInfo']), dtype=object).flat)
            channelId       = list(np.array(np.concatenate(resultsInSampling['channelId']), dtype=object).flat)
            channelGain     = list(np.array(np.concatenate(resultsInSampling['channelGain']), dtype=object).flat)
            channelEta      = list(np.array(np.concatenate(resultsInSampling['channelEta']), dtype=object).flat)
            channelPhi      = list(np.array(np.concatenate(resultsInSampling['channelPhi']), dtype=object).flat)
            channelShape    = list(resultsInSampling['channelShape'])
            channelShapeDer = list(resultsInSampling['channelShapeDer'])

            print(np.shape(channelShape), np.shape(channelShapeDer))

            allShapesInSampling     = []
            allShapesDerInSampling  = []
            allDigitsInSampling     = []
            for shapeClus, shapeDerClus, digClus in zip(channelShape, channelShapeDer, digits):
                for shapeCell, shapeDerCell, digCell in zip(shapeClus,shapeDerClus, digClus):
                    allShapesInSampling.append(shapeCell)
                    allShapesDerInSampling.append(shapeDerCell)
                    allDigitsInSampling.append(digCell)
                

            myFig = plt.figure(figsize=(8,6))
            theBins     = np.histogram(channelEnergy,xbins)[1]
            plt.hist(digitsSize,bins=xbins,histtype='stepfilled',edgecolor='black',color='gray', alpha=0.2,label='Digits size', density=False)
            plt.title('digitsSize {} at {}.'.format(clusName, layer)+r' $e^{\pm}$$_{cut} =$'+' {} GeV'.format(el_thr))
            # plt.xlabel('Energy [MeV]',fontsize=labelFontSize)
            # plt.ylabel("counts",fontsize=labelFontSize)
            # plt.yscale('log')
            plt.legend()
            filename = datasetPath+'digitsSize_{}_{}.png'.format(layer, clusName)
            plt.savefig(filename)
            plt.close(myFig)

            myFig = plt.figure(figsize=(8,6))
            theBins     = np.histogram(channelEnergy,xbins)[1]
            plt.hist(channelEnergy,bins=theBins,histtype='stepfilled',edgecolor='black',color='gray', alpha=0.2,label='Energy', density=False)
            plt.title('Energy from cells in {} at {}.'.format(clusName, layer)+r' $e^{\pm}$$_{cut} =$'+' {} GeV'.format(el_thr))
            # plt.xlabel('Energy [MeV]',fontsize=labelFontSize)
            # plt.ylabel("counts",fontsize=labelFontSize)
            # plt.yscale('log')
            plt.legend()
            filename = datasetPath+'ene_{}_{}.png'.format(layer, clusName)
            plt.savefig(filename)
            plt.close(myFig)

            myFig = plt.figure(figsize=(8,6))
            theBins     = np.histogram(channelTime,xbins)[1]
            plt.hist(channelTime,bins=theBins,histtype='stepfilled',edgecolor='black',color='gray', alpha=0.2,label='Time', density=False)
            plt.title('Time from cells in {} at {}.'.format(clusName, layer)+r' $e^{\pm}$$_{cut} =$'+' {} GeV'.format(el_thr))
            # plt.xlabel('Energy [MeV]',fontsize=labelFontSize)
            # plt.ylabel("counts",fontsize=labelFontSize)
            # plt.yscale('log')
            plt.legend()
            filename = datasetPath+'time_{}_{}.png'.format(layer, clusName)
            plt.savefig(filename)
            plt.close(myFig)

            myFig = plt.figure(figsize=(8,6))
            for k in range(0, len(allShapesInSampling)):
                plt.plot(np.arange(len(allShapesInSampling[k])),allShapesInSampling[k],'.-')
            plt.title('Shape from cells in {} at {}.'.format(clusName, layer)+r' $e^{\pm}$$_{cut} =$'+' {} GeV'.format(el_thr))
            # plt.xlabel('Energy [MeV]',fontsize=labelFontSize)
            # plt.ylabel("counts",fontsize=labelFontSize)
            # plt.yscale('log')
            # plt.legend()
            filename = datasetPath+'shape_{}_{}.png'.format(layer, clusName)
            plt.savefig(filename)
            plt.close(myFig)

            myFig = plt.figure(figsize=(8,6))
            for k in range(0, len(allShapesDerInSampling)):
                plt.plot(np.arange(len(allShapesDerInSampling[k])),allShapesDerInSampling[k],'.-')
            plt.title('Shape Derivative from cells in {} at {}.'.format(clusName, layer)+r' $e^{\pm}$$_{cut} =$'+' {} GeV'.format(el_thr))
            # plt.xlabel('Energy [MeV]',fontsize=labelFontSize)
            # plt.ylabel("counts",fontsize=labelFontSize)
            # plt.yscale('log')
            # plt.legend()
            filename = datasetPath+'shapeDer_{}_{}.png'.format(layer, clusName)
            plt.savefig(filename)
            plt.close(myFig)