### Data ####

setupATLAS
asetup Athena,21.0.30
# lsetup rucio
lsetup panda

# Reco test 02.2: (Success 100% !!) DS 08
pathena --trf "Reco_tf.py  --inputBSFile=%IN --outputESDFile=%OUT.ESD.pool.root --AMIConfig=r9738 --AMITag=r9738 --maxEvents=3500 --AddCaloDigi=True --postExec='RAWtoESD:StreamESD.ItemList+=[\"TileDigitsContainer#TileDigitsCnt\",\"LArDigitContainer#FREE\",\"LArRawChannelContainer#LArRawChannels\",\"TileRawChannelContainer#TileRawChannelCnt\"]' " --inDS=user.mhufnage:user.mhufnage.data17_13TeV.00329542.MinBias.daq.RAW_dataset08 --outDS=user.mhufnage.data17_13TeV.00329542.physics_MinBias.daq.RAW._dataset08_v01_2023 --noBuild --nGBPerJob 5 --nEventsPerFile 3500 --nCore 4
jediTaskID=33593694

# Reco test 02.3: (Success 100% !!) DS 09
pathena --trf "Reco_tf.py  --inputBSFile=%IN --outputESDFile=%OUT.ESD.pool.root --AMIConfig=r9738 --AMITag=r9738 --maxEvents=3500 --AddCaloDigi=True --postExec='RAWtoESD:StreamESD.ItemList+=[\"TileDigitsContainer#TileDigitsCnt\",\"LArDigitContainer#FREE\",\"LArRawChannelContainer#LArRawChannels\",\"TileRawChannelContainer#TileRawChannelCnt\"]' " --inDS=user.mhufnage:user.mhufnage.data17_13TeV.00329542.MinBias.daq.RAW_dataset09 --outDS=user.mhufnage.data17_13TeV.00329542.physics_MinBias.daq.RAW._dataset09_v00_2023 --noBuild --nGBPerJob 5 --nEventsPerFile 3500 --nCore 4 --respectSplitRule
jediTaskID=33593728

# Reco test 02.4: () DS 10
pathena --trf "Reco_tf.py  --inputBSFile=%IN --outputESDFile=%OUT.ESD.pool.root --AMIConfig=r9738 --AMITag=r9738 --maxEvents=3500 --AddCaloDigi=True --postExec='RAWtoESD:StreamESD.ItemList+=[\"TileDigitsContainer#TileDigitsCnt\",\"LArDigitContainer#FREE\",\"LArRawChannelContainer#LArRawChannels\",\"TileRawChannelContainer#TileRawChannelCnt\"]' " --inDS=user.mhufnage:user.mhufnage.data17_13TeV.00329542.MinBias.daq.RAW_dataset10 --outDS=user.mhufnage.data17_13TeV.00329542.physics_MinBias.daq.RAW._dataset10_v00_2023 --noBuild --nGBPerJob 5 --nEventsPerFile 3500 --nCore 4 --respectSplitRule

# Reco test 02.5: () DS 11
pathena --trf "Reco_tf.py  --inputBSFile=%IN --outputESDFile=%OUT.ESD.pool.root --AMIConfig=r9738 --AMITag=r9738 --maxEvents=3500 --AddCaloDigi=True --postExec='RAWtoESD:StreamESD.ItemList+=[\"TileDigitsContainer#TileDigitsCnt\",\"LArDigitContainer#FREE\",\"LArRawChannelContainer#LArRawChannels\",\"TileRawChannelContainer#TileRawChannelCnt\"]' " --inDS=user.mhufnage:user.mhufnage.data17_13TeV.00329542.MinBias.daq.RAW_dataset11 --outDS=user.mhufnage.data17_13TeV.00329542.physics_MinBias.daq.RAW._dataset11_v00_2023 --noBuild --nGBPerJob 5 --nEventsPerFile 3500 --nCore 4 --respectSplitRule

# Reco test 02.6: () DS 12
pathena --trf "Reco_tf.py  --inputBSFile=%IN --outputESDFile=%OUT.ESD.pool.root --AMIConfig=r9738 --AMITag=r9738 --maxEvents=3500 --AddCaloDigi=True --postExec='RAWtoESD:StreamESD.ItemList+=[\"TileDigitsContainer#TileDigitsCnt\",\"LArDigitContainer#FREE\",\"LArRawChannelContainer#LArRawChannels\",\"TileRawChannelContainer#TileRawChannelCnt\"]' " --inDS=user.mhufnage:user.mhufnage.data17_13TeV.00329542.MinBias.daq.RAW_dataset12 --outDS=user.mhufnage.data17_13TeV.00329542.physics_MinBias.daq.RAW._dataset12_v00_2023 --noBuild --nGBPerJob 5 --nEventsPerFile 3500 --nCore 4 --respectSplitRule

# Reco test 02.7: () DS 13
pathena --trf "Reco_tf.py  --inputBSFile=%IN --outputESDFile=%OUT.ESD.pool.root --AMIConfig=r9738 --AMITag=r9738 --maxEvents=3500 --AddCaloDigi=True --postExec='RAWtoESD:StreamESD.ItemList+=[\"TileDigitsContainer#TileDigitsCnt\",\"LArDigitContainer#FREE\",\"LArRawChannelContainer#LArRawChannels\",\"TileRawChannelContainer#TileRawChannelCnt\"]' " --inDS=user.mhufnage:user.mhufnage.data17_13TeV.00329542.MinBias.daq.RAW_dataset13 --outDS=user.mhufnage.data17_13TeV.00329542.physics_MinBias.daq.RAW._dataset13_v00_2023 --noBuild --nGBPerJob 5 --nEventsPerFile 3500 --nCore 4 --respectSplitRule

# Reco test 02.8: () DS 01
pathena --trf "Reco_tf.py  --inputBSFile=%IN --outputESDFile=%OUT.ESD.pool.root --AMIConfig=r9738 --AMITag=r9738 --maxEvents=3500 --AddCaloDigi=True --postExec='RAWtoESD:StreamESD.ItemList+=[\"TileDigitsContainer#TileDigitsCnt\",\"LArDigitContainer#FREE\",\"LArRawChannelContainer#LArRawChannels\",\"TileRawChannelContainer#TileRawChannelCnt\"]' " --inDS=user.mhufnage:user.mhufnage.data17_13TeV.00329542.MinBias.daq.RAW_dataset01 --outDS=user.mhufnage.data17_13TeV.00329542.physics_MinBias.daq.RAW._dataset01_v02_2023 --noBuild --nGBPerJob 5 --nEventsPerFile 3500 --nCore 4

# Reco test 02.9: () DS 02
pathena --trf "Reco_tf.py  --inputBSFile=%IN --outputESDFile=%OUT.ESD.pool.root --AMIConfig=r9738 --AMITag=r9738 --maxEvents=3500 --AddCaloDigi=True --postExec='RAWtoESD:StreamESD.ItemList+=[\"TileDigitsContainer#TileDigitsCnt\",\"LArDigitContainer#FREE\",\"LArRawChannelContainer#LArRawChannels\",\"TileRawChannelContainer#TileRawChannelCnt\"]' " --inDS=user.mhufnage:user.mhufnage.data17_13TeV.00329542.MinBias.daq.RAW_dataset02 --outDS=user.mhufnage.data17_13TeV.00329542.physics_MinBias.daq.RAW._dataset02_v02_2023 --noBuild --nGBPerJob 5 --nEventsPerFile 3500 --nCore 4

# Reco test 02.10: () DS 03
pathena --trf "Reco_tf.py  --inputBSFile=%IN --outputESDFile=%OUT.ESD.pool.root --AMIConfig=r9738 --AMITag=r9738 --maxEvents=3500 --AddCaloDigi=True --postExec='RAWtoESD:StreamESD.ItemList+=[\"TileDigitsContainer#TileDigitsCnt\",\"LArDigitContainer#FREE\",\"LArRawChannelContainer#LArRawChannels\",\"TileRawChannelContainer#TileRawChannelCnt\"]' " --inDS=user.mhufnage:user.mhufnage.data17_13TeV.00329542.MinBias.daq.RAW_dataset03 --outDS=user.mhufnage.data17_13TeV.00329542.physics_MinBias.daq.RAW._dataset03_v02_2023 --noBuild --nGBPerJob 5 --nEventsPerFile 3500 --nCore 4

# Reco test 02.11: () DS 04
pathena --trf "Reco_tf.py  --inputBSFile=%IN --outputESDFile=%OUT.ESD.pool.root --AMIConfig=r9738 --AMITag=r9738 --maxEvents=3500 --AddCaloDigi=True --postExec='RAWtoESD:StreamESD.ItemList+=[\"TileDigitsContainer#TileDigitsCnt\",\"LArDigitContainer#FREE\",\"LArRawChannelContainer#LArRawChannels\",\"TileRawChannelContainer#TileRawChannelCnt\"]' " --inDS=user.mhufnage:user.mhufnage.data17_13TeV.00329542.MinBias.daq.RAW_dataset04 --outDS=user.mhufnage.data17_13TeV.00329542.physics_MinBias.daq.RAW._dataset04_v02_2023 --noBuild --nGBPerJob 5 --nEventsPerFile 3500 --nCore 4

# Reco test 02.12: () DS 05
pathena --trf "Reco_tf.py  --inputBSFile=%IN --outputESDFile=%OUT.ESD.pool.root --AMIConfig=r9738 --AMITag=r9738 --maxEvents=3500 --AddCaloDigi=True --postExec='RAWtoESD:StreamESD.ItemList+=[\"TileDigitsContainer#TileDigitsCnt\",\"LArDigitContainer#FREE\",\"LArRawChannelContainer#LArRawChannels\",\"TileRawChannelContainer#TileRawChannelCnt\"]' " --inDS=user.mhufnage:user.mhufnage.data17_13TeV.00329542.MinBias.daq.RAW_dataset05 --outDS=user.mhufnage.data17_13TeV.00329542.physics_MinBias.daq.RAW._dataset05_v01_2023 --noBuild --nGBPerJob 5 --nEventsPerFile 3500 --nCore 4

# Reco test 02.13: () DS 06
pathena --trf "Reco_tf.py  --inputBSFile=%IN --outputESDFile=%OUT.ESD.pool.root --AMIConfig=r9738 --AMITag=r9738 --maxEvents=3500 --AddCaloDigi=True --postExec='RAWtoESD:StreamESD.ItemList+=[\"TileDigitsContainer#TileDigitsCnt\",\"LArDigitContainer#FREE\",\"LArRawChannelContainer#LArRawChannels\",\"TileRawChannelContainer#TileRawChannelCnt\"]' " --inDS=user.mhufnage:user.mhufnage.data17_13TeV.00329542.MinBias.daq.RAW_dataset06 --outDS=user.mhufnage.data17_13TeV.00329542.physics_MinBias.daq.RAW._dataset06_v02_2023 --noBuild --nGBPerJob 5 --nEventsPerFile 3500 --nCore 4

# Reco test 02.14: () DS 07
pathena --trf "Reco_tf.py  --inputBSFile=%IN --outputESDFile=%OUT.ESD.pool.root --AMIConfig=r9738 --AMITag=r9738 --maxEvents=3500 --AddCaloDigi=True --postExec='RAWtoESD:StreamESD.ItemList+=[\"TileDigitsContainer#TileDigitsCnt\",\"LArDigitContainer#FREE\",\"LArRawChannelContainer#LArRawChannels\",\"TileRawChannelContainer#TileRawChannelCnt\"]' " --inDS=user.mhufnage:user.mhufnage.data17_13TeV.00329542.MinBias.daq.RAW_dataset07 --outDS=user.mhufnage.data17_13TeV.00329542.physics_MinBias.daq.RAW._dataset07_v01_2023 --noBuild --nGBPerJob 5 --nEventsPerFile 3500 --nCore 4


############
### MC #####
############
setupATLAS
lsetup panda
lsetup rucio

# Reco test 01: (FAIL)
asetup Athena,22.0.37
pathena --trf "Reco_tf.py --athenaopts=' --threads=1 --imf --perfmon' --AddCaloDigi True --inputHitsFile=%IN --outputRDOFile=tmp.RDO.pool.root; Reco_tf.py  --athenaopts=' --threads=1 --imf --perfmon' --AddCaloDigi=True --inputRDOFile=tmp.RDO.pool.root --outputESDFile=%OUT.ESD.pool.root --postExec='StreamESD.ItemList+=[\"TileDigitsContainer#TileDigitsCnt\", \"LArDigitContainer#FREE\", \"LArDigitContainer#LArDigitContainer_Thinned\", \"LArRawChannelContainer#LArRawChannels\", \"TileRawChannelContainer#TileRawChannelCnt\"]' " --inDS=mc16_13TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.simul.HITS.e3601_e5984_s3240_tid12832049_00 --outDS=user.mhufnage.mc16_13TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.simul.HITS.e3601_e5984_s3240_tid12832049_00.v0_2023 --noBuild
jediTaskID=33447808


# Reco test 02: (FAIL)
asetup Athena,21.3.19
pathena --trf "Reco_tf.py --AMIConfig=r11881 --AMITag=r11881 --AddCaloDigi True --inputHitsFile=%IN --outputRDOFile=tmp.RDO.pool.root; Reco_tf.py  --athenaopts=' --threads=1 --imf --perfmon' --AddCaloDigi=True --inputRDOFile=tmp.RDO.pool.root --outputESDFile=%OUT.ESD.pool.root --postExec='StreamESD.ItemList+=[\"TileDigitsContainer#TileDigitsCnt\", \"LArDigitContainer#FREE\", \"LArDigitContainer#LArDigitContainer_Thinned\", \"LArRawChannelContainer#LArRawChannels\", \"TileRawChannelContainer#TileRawChannelCnt\"]' " --inDS=mc16_13TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.simul.HITS.e3601_e5984_s3240_tid12832049_00 --outDS=user.mhufnage.mc16_13TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.simul.HITS.e3601_e5984_s3240_tid12832049_00.v1_2023 --noBuild
jediTaskID=33452511

# Reco test 03: (FAIL)
asetup Athena,21.3.19
pathena --trf "Reco_tf.py --AMIConfig=r11881 --AMITag=r11881 --AddCaloDigi True --inputHitsFile=%IN --outputRDOFile=tmp.RDO.pool.root; Reco_tf.py  --athenaopts=' --threads=1 --imf --perfmon' --AddCaloDigi=True --inputRDOFile=tmp.RDO.pool.root --outputESDFile=%OUT.ESD.pool.root --postExec='RAWtoESD:StreamESD.ItemList+=[\"TileDigitsContainer#TileDigitsCnt\", \"LArDigitContainer#FREE\", \"LArDigitContainer#LArDigitContainer_Thinned\", \"LArRawChannelContainer#LArRawChannels\", \"TileRawChannelContainer#TileRawChannelCnt\"]' " --inDS=mc16_13TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.simul.HITS.e3601_e5984_s3240_tid12832049_00 --outDS=user.mhufnage.mc16_13TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.simul.HITS.e3601_e5984_s3240_tid12832049_00.v2_2023 --noBuild
jediTaskID=33456666

# Reco test 04: (FAIL)
asetup Athena,21.3.19
pathena --trf "Reco_tf.py --AddCaloDigi True --inputHitsFile=%IN --outputRDOFile=tmp.RDO.pool.root; Reco_tf.py  --athenaopts=' --threads=1 --imf --perfmon' --AddCaloDigi=True --inputRDOFile=tmp.RDO.pool.root --outputESDFile=%OUT.ESD.pool.root --postExec='RAWtoESD:StreamESD.ItemList+=[\"TileDigitsContainer#TileDigitsCnt\", \"LArDigitContainer#FREE\", \"LArDigitContainer#LArDigitContainer_Thinned\", \"LArRawChannelContainer#LArRawChannels\", \"TileRawChannelContainer#TileRawChannelCnt\"]' " --inDS=mc16_13TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.simul.HITS.e3601_e5984_s3240_tid12832049_00 --outDS=user.mhufnage.mc16_13TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.simul.HITS.e3601_e5984_s3240_tid12832049_00.v3_2023 --noBuild
jediTaskID=33456716

# Reco test 05:
#-> Split reco into HITS->RDO  (FAIL)
asetup Athena,21.3.19
pathena --trf "Reco_tf.py --AddCaloDigi True --inputHitsFile=%IN --outputRDOFile=%OUT.RDO.pool.root " --inDS=mc16_13TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.simul.HITS.e3601_e5984_s3240_tid12832049_00 --outDS=user.mhufnage.mc16_13TeV.361106.Zee.dig.RDO.v0_2023 --noBuild
jediTaskID=33457172

# ... and RDO->ESD steps.
asetup Athena,21.3.19
pathena --trf "Reco_tf.py --AddCaloDigi True --inputRDOFile=%IN --outputESDFile=%OUT.ESD.pool.root --postExec='StreamESD.ItemList+=[\"TileDigitsContainer#TileDigitsCnt\", \"LArDigitContainer#FREE\", \"LArDigitContainer#LArDigitContainer_Thinned\", \"LArRawChannelContainer#LArRawChannels\", \"TileRawChannelContainer#TileRawChannelCnt\"]' " --inDS=user.mhufnage.mc16_13TeV.361106.Zee.dig.RDO.v0_2023 --outDS=user.mhufnage.mc16_13TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.simul.ESD.e3601_e5984_s3240_tid12832049_00.v3_2023 --noBuild

# Reco Test 06: (old command) (FAIL)
asetup Athena,22.0.44
pathena --trf "Reco_tf.py --athenaopts=' --threads=1 --imf --perfmon' --AddCaloDigi True --inputHitsFile=%IN --outputRDOFile=tmp.RDO.pool.root; Reco_tf.py --athenaopts=' --threads=1 --imf --perfmon' --AddCaloDigi=True --inputRDOFile=tmp.RDO.pool.root --outputESDFile=%OUT.ESD.pool.root --postExec='StreamESD.ItemList+=["TileDigitsContainer#TileDigitsCnt", "LArDigitContainer#FREE", "LArDigitContainer#LArDigitContainer_Thinned", "LArRawChannelContainer#LArRawChannels", "TileRawChannelContainer#TileRawChannelCnt"]' " --inDS=mc16_13TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.simul.HITS.e3601_e5984_s3240_tid12832049_00 --outDS=user.mhufnage.mc16_13TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.simul.HITS.e3601_e5984_s3240.v5_2023 --noBuild
jediTaskID=33470158

#------------------------------------
# LOCAL reco test 01: (SUCCESS 1/1)
#------------------------------------
asetup Athena,22.0.44
Reco_tf.py --maxEvents=-1 --athenaopts=' --threads=1 --imf --perfmon' --AddCaloDigi True --inputHitsFile=HITS.12832049._010826.pool.root.1 --outputRDOFile=RDO.pool.root > reco.log 2>&1
Reco_tf.py --athenaopts=' --threads=1 --imf --perfmon' --AddCaloDigi=True --inputRDOFile=RDO.pool.root --outputESDFile=ESD.pool.root --postExec='StreamESD.ItemList+=["TileDigitsContainer#TileDigitsCnt", "LArDigitContainer#FREE", "LArDigitContainer#LArDigitContainer_Thinned", "LArRawChannelContainer#LArRawChannels", "TileRawChannelContainer#TileRawChannelCnt"]' > reco.log 2>&1

asetup Athena,21.0.30
Reco_tf.py  --inputBSFile=/eos/user/m/mhufnage/ALP_project/RAW/user.mhufnage.data17_13TeV.00329542.physics_MinBias.daq.RAW_der1655905167/data17_13TeV.00329542.physics_MinBias.daq.RAW._lb0327._SFO-3._0004.data --outputESDFile=ESD.pool.root --AMIConfig=r9738 --AMITag=r9738 --maxEvents=5 --skipEvents=0 --AddCaloDigi=True --postExec='StreamESD.ItemList+=["TileDigitsContainer#TileDigitsCnt", "LArDigitContainer#FREE", "LArDigitContainer#LArDigitContainer_Thinned", "LArRawChannelContainer#LArRawChannels", "TileRawChannelContainer#TileRawChannelCnt"]' > reco.log 2>&1

asetup Athena,21.0.30
Reco_tf.py  --inputBSFile=/eos/user/m/mhufnage/ALP_project/RAW/user.mhufnage.data17_13TeV.00329542.physics_MinBias.daq.RAW_der1655905167/data17_13TeV.00329542.physics_MinBias.daq.RAW._lb0327._SFO-3._0004.data --outputESDFile=ESD.pool.root --AMIConfig=r9738 --AMITag=r9738 --maxEvents=5 --skipEvents=4 --AddCaloDigi=True --postExec='StreamESD.ItemList+=["TileDigitsContainer#TileDigitsCnt", "LArDigitContainer#FREE", "LArDigitContainer#LArDigitContainer_Thinned", "LArRawChannelContainer#LArRawChannels", "TileRawChannelContainer#TileRawChannelCnt"]' > reco2.log 2>&1

#------------------------------------------------------------------------
# Reco Test 07: (HITtoRDO)
asetup Athena,22.0.44
pathena --trf "Reco_tf.py --athenaopts=' --threads=1 --imf --perfmon' --AddCaloDigi True --inputHITSFile=%IN --outputRDOFile=%OUT.RDO.root " --inDS=mc16_13TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.simul.HITS.e3601_e5984_s3240_tid12832049_00 --outDS=user.mhufnage.mc16_13TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.simul.RDO.e3601_e5984_s3240.v7_2023 --noBuild
jediTaskID=33473621
retry(33473621,nEventsPerJob=1000,nGBPerJob=5,nFilesPerJob=1)


# --------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------------------------------------------------------------------------------

# A rodar... 25/07/2022 (crash)
# pathena --inDS=data17_13TeV.00329542.physics_MinBias.daq.RAW --outDS=user.mhufnage.data17_13TeV.00329542.physics_MinBias.daq.RAW.v0 --trf "Reco_tf.py --AMIConfig=r9738 --AMITag=r9738 --maxEvents=-1 --AddCaloDigi=True --inputBSFile=%IN --outputESDFile=%OUT.ESD.pool.root --postExec='RAWtoESD:StreamESD.ItemList+=['TileDigitsContainer#TileDigitsCnt']; StreamESD.ItemList+=['LArDigitContainer#*']; StreamESD.ItemList+=['LArRawChannelContainer#LArRawChannels']; StreamESD.ItemList+=['TileRawChannelContainer#TileRawChannelCnt'];'"
# A testar local... 26/07/2022 (ok)
# setupATLAS
# asetup Athena,21.0.30
# Reco_tf.py  --inputBSFile=/eos/user/m/mhufnage/ALP_project/RAW/user.mhufnage.data17_13TeV.00329542.physics_MinBias.daq.RAW_der1655905167/data17_13TeV.00329542.physics_MinBias.daq.RAW._lb0239._SFO-4._0001.data --outputESDFile=ESD_minBias_.pool.root --AMIConfig=r9738 --AMITag=r9738 --maxEvents=5 --AddCaloDigi=True --postExec="RAWtoESD:StreamESD.ItemList+=[\"TileDigitsContainer#TileDigitsCnt\"];         StreamESD.ItemList+=[\"LArDigitContainer#*\"]; StreamESD.ItemList+=[\"LArRawChannelContainer#LArRawChannels\"]; StreamESD.ItemList+=[\"TileRawChannelContainer#TileRawChannelCnt\"];"

# A testar grid... 26/07/2022 (crash StreamESD)
pathena --inDS=data17_13TeV.00329542.physics_MinBias.daq.RAW --outDS=user.mhufnage.data17_13TeV.00329542.physics_MinBias.daq.RAW.v0 --trf 'Reco_tf.py  --inputBSFile=%IN --outputESDFile=%OUT.ESD.pool.root --AMIConfig=r9738 --AMITag=r9738 --maxEvents=5 --AddCaloDigi=True --postExec="RAWtoESD:StreamESD.ItemList+=[\"TileDigitsContainer#TileDigitsCnt\"]; StreamESD.ItemList+=[\"LArDigitContainer#*\"]; StreamESD.ItemList+=[\"LArRawChannelContainer#LArRawChannels\"]; StreamESD.ItemList+=[\"TileRawChannelContainer#TileRawChannelCnt\"];"'
## &&&&&&&&&&&&
# A testar grid... 27/07/2022
setupATLAS
asetup Athena,21.0.30
lsetup rucio
lsetup panda
# pathena --inDS=data17_13TeV.00329542.physics_MinBias.daq.RAW --outDS=user.mhufnage.data17_13TeV.00329542.physics_MinBias.daq.RAW.v3 --trf "Reco_tf.py  --inputBSFile=%IN --outputESDFile=%OUT.ESD.pool.root --AMIConfig=r9738 --AMITag=r9738 --maxEvents=-1 --AddCaloDigi=True --postExec='RAWtoESD:StreamESD.ItemList+=['TileDigitsContainer#TileDigitsCnt']; StreamESD.ItemList+=['LArDigitContainer#*']; StreamESD.ItemList+=['LArRawChannelContainer#LArRawChannels']; StreamESD.ItemList+=['TileRawChannelContainer#TileRawChannelCnt'];'"
# pathena --inDS=data17_13TeV.00329542.physics_MinBias.daq.RAW --outDS=user.mhufnage.data17_13TeV.00329542.physics_MinBias.daq.RAW.v4 --trf "Reco_tf.py  --inputBSFile=%IN --outputESDFile=%OUT.ESD.pool.root --AMIConfig=r9738 --AMITag=r9738 --maxEvents=-1 --AddCaloDigi=True --postExec=\"RAWtoESD:StreamESD.ItemList+=['TileDigitsContainer#TileDigitsCnt']; StreamESD.ItemList+=['LArDigitContainer#*']; StreamESD.ItemList+=['LArRawChannelContainer#LArRawChannels']; StreamESD.ItemList+=['TileRawChannelContainer#TileRawChannelCnt'];\""
pathena --trf "Reco_tf.py  --inputBSFile=%IN --outputESDFile=%OUT.ESD.pool.root --AMIConfig=r9738 --AMITag=r9738 --maxEvents=-1 --AddCaloDigi=True --postExec='RAWtoESD:StreamESD.ItemList+=[\"TileDigitsContainer#TileDigitsCnt\", \"LArDigitContainer#FREE\", \"LArDigitContainer#LArDigitContainer_Thinned\", \"LArRawChannelContainer#LArRawChannels\", \"TileRawChannelContainer#TileRawChannelCnt\"]' " --inDS=data17_13TeV.00329542.physics_MinBias.daq.RAW --outDS=user.mhufnage.data17_13TeV.00329542.physics_MinBias.daq.RAW.v4
rucio list-dids user.mhufnage:*

## &&&&&&&&&&&&



# pathena --trf "Reco_tf.py --AMIConfig=r9738 --AMITag=r9738 --maxEvents=-1 --AddCaloDigi=True --inputBSFile=%IN --outputESDFile=%OUT.ESD.pool.root" --inDS=data17_13TeV.00329542.physics_MinBias.daq.RAW --outDS=user.mhufnage.data17_13TeV.00329542.physics_MinBias.daq.RAW.v0


pathena --trf "Reco_tf.py --CA --steering doRAWtoALL --maxEvents=-1 --inputHITSFile=%IN --outputAODFile=%OUT.AOD.pool.root --preInclude egammaConfig.egammaOnlyFromRawFlags.egammaOnlyFromRaw --preExec='from AthenaConfiguration.AllConfigFlags import ConfigFlags;'" --inDS=mc21_13p6TeV.801278.Py8EG_A14NNPDF23LO_perf_JF17.simul.HITS.e8400_e7400_s3775 --outDS=user.eegidiop.mc21_13p6TeV.801278.Py8EG_A14NNPDF23LO_perf_JF17.simul.HITS.e8400_e7400_s3775.v0

# pathena --trf "Reco_tf.py --CA --steering doRAWtoALL --maxEvents=-1 --inputHITSFile=%IN --outputAODFile=%OUT.AOD.pool.root --preInclude egammaConfig.egammaOnlyFromRawFlags.egammaOnlyFromRaw --preExec='from AthenaConfiguration.AllConfigFlags import ConfigFlags;'" --inDS=mc21_13p6TeV.801278.Py8EG_A14NNPDF23LO_perf_JF17.simul.HITS.e8400_e7400_s3775 --outDS=user.mhufnage.mc21_13p6TeV.801278.Py8EG_A14NNPDF23LO_perf_JF17.simul.HITS.e8400_e7400_s3775.v0

# pathena --trf "Reco_tf.py --AMIConfig=r9738 --AMITag=r9738 --maxEvents=-1 --AddCaloDigi=True --inputBSFile=%IN --outputESDFile=%OUT.ESD.pool.root --postExec='RAWtoESD:StreamESD.ItemList+=[\"TileDigitsContainer#TileDigitsCnt\"]; StreamESD.ItemList+=[\"LArDigitContainer#*\"]; StreamESD.ItemList+=[\"LArRawChannelContainer#LArRawChannels\"]; StreamESD.ItemList+=[\"TileRawChannelContainer#TileRawChannelCnt\"];' --inDS=data17_13TeV.00329542.physics_MinBias.daq.RAW --outDS=user.mhufnage.data17_13TeV.00329542.physics_MinBias.daq.RAW.v0"

# Reco_tf.py  \
#         --AMIConfig=r9738 \
#         --AMITag=r9738 \
#         --AddCaloDigi True \
#         --maxEvents $nEvents \
#         --inputBSFile=$file \
#         --outputESDFile=ESD_minBias_$indexOutput.pool.root \
#         --outputAODFile=AOD_minBias_$indexOutput.pool.root \
#         --postExec="RAWtoESD:StreamESD.ItemList+=[\"TileDigitsContainer#TileDigitsCnt\"];\
#         StreamESD.ItemList+=[\"LArDigitContainer#*\"];\
#         StreamESD.ItemList+=[\"LArRawChannelContainer#LArRawChannels\"];\
#         StreamESD.ItemList+=[\"TileRawChannelContainer#TileRawChannelCnt\"];\
#         "\
#         >Reco_tf_$indexOutput.log 2>&1