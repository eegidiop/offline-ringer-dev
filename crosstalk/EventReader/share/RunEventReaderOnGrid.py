#!/usr/bin/env python
#
#  Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
#   mateus.hufnagel@cern.ch
#   edmar.egidio@cern.ch



import argparse
import os
from AthenaCommon.Logging import logging
mainLogger = logging.getLogger( 'RunEventReaderOnGrid' )


parser = argparse.ArgumentParser(description = '', add_help = False)
parser = argparse.ArgumentParser()


#
# prun configuration
#

parser.add_argument('--inDS', action='store', dest='inDS', required = True, type=str, help = "Input dataset.")

parser.add_argument('--outDS', action='store', dest='outDS', required = True, type=str, help = "Output dataset.")

parser.add_argument('--nFilesPerJob', action='store', dest='nFilesPerJob', required = False, type=int, default=1,
                    help = "Number of files per job in grid mode.")

parser.add_argument('--job', action='store', dest='job', required = False, type=str, default='source build/x86_64-centos7-gcc8-opt/setup.sh;athena.py EventReader/share/EventReader_crosstalk_gridJobOptions.py',
                    help = "Job commands to be executed")

parser.add_argument('--a', '--asetup', action='store', dest='asetup', required = False, type=str, default='asetup Athena,22.0.44',
                    help = "asetup release")

#
# Job Command
#

import sys,os
if len(sys.argv)==1:
  parser.print_help()
  sys.exit(1)


args = parser.parse_args()

execute = "{JOB} --filesInput='%IN'".format(JOB=args.job)

command = """ prun --exec \
       "{COMMAND};" \
     --inDS={INDS} \
     --outDS={OUTDS} \
     --disableAutoRetry \
     --outputs="*.root" \
     --useAthenaPackages \
     --nGBPerJob=MAX \
     --cmtConfig=x86_64-centos7-gcc8-opt \
     --useHomeDir \
     --gluePackages EventReader \
     --bexec "make" \
     --extFile /build/x86_64-centos7-gcc8-opt/lib/libEventReader.so,build/x86_64-centos7-gcc8-opt/lib/libEventReader.so.dbg,/build/EventReader/CMakeFiles/EventReader.dir/Root/EventReaderAlg.cxx.o,/build/EventReader/CMakeFiles/EventReader.dir/Root/components/EventReader_entries.cxx.o,/build/EventReader/CMakeFiles/EventReader.dir/Root/components/EventReader_load.cxx.o

    """

command = command.format( 
                            INDS      = args.inDS,
                            OUTDS     = args.outDS,
                            COMMAND   = execute,
                            # N_FILES_PER_JOB = args.nFilesPerJob,
                            )



print(command)
# os.system(command) # -> Uncomment to apply command directly to terminal !


## Successful commands: 
#1# prun --exec        "source build/x86_64-centos7-gcc8-opt/setup.sh;athena.py EventReader/share/EventReader_crosstalk_gridJobOptions.py --filesInput='%IN';"    --inDS=user.mhufnage.data17_13TeV.00329542.physics_MinBias.daq.RAW._dataset04_v02_2023_EXT0.490839543      --outDS=user.mhufnage.data17_13TeV_testEventReader19.5      --disableAutoRetry      --useAthenaPackages      --outputs="*.root" --nGBPerJob=MAX      --cmtConfig=x86_64-centos7-gcc8-opt --useHomeDir --gluePackages EventReader --bexec "make" --extFile /build/x86_64-centos7-gcc8-opt/lib/libEventReader.so,build/x86_64-centos7-gcc8-opt/lib/libEventReader.so.dbg,/build/EventReader/CMakeFiles/EventReader.dir/Root/EventReaderAlg.cxx.o,/build/EventReader/CMakeFiles/EventReader.dir/Root/components/EventReader_entries.cxx.o,/build/EventReader/CMakeFiles/EventReader.dir/Root/components/EventReader_load.cxx.o 

#2# prun --exec        "source build/x86_64-centos7-gcc8-opt/setup.sh;athena.py EventReader/share/EventReader_crosstalk_gridJobOptions.py --filesInput='%IN';"    --inDS=user.mhufnage.data17_13TeV.00329542.physics_MinBias.daq.RAW._dataset01_v02_2023_EXT0,user.mhufnage.data17_13TeV.00329542.physics_MinBias.daq.RAW._dataset02_v02_2023_EXT0,user.mhufnage.data17_13TeV.00329542.physics_MinBias.daq.RAW._dataset03_v02_2023_EXT0,user.mhufnage.data17_13TeV.00329542.physics_MinBias.daq.RAW._dataset04_v02_2023_EXT0,user.mhufnage.data17_13TeV.00329542.physics_MinBias.daq.RAW._dataset05_v01_2023_EXT0,user.mhufnage.data17_13TeV.00329542.physics_MinBias.daq.RAW._dataset06_v02_2023_EXT0,user.mhufnage.data17_13TeV.00329542.physics_MinBias.daq.RAW._dataset07_v02_2023_EXT0,user.mhufnage.data17_13TeV.00329542.physics_MinBias.daq.RAW._dataset08_v01_2023_EXT0,user.mhufnage.data17_13TeV.00329542.physics_MinBias.daq.RAW._dataset09_v00_2023_EXT0,user.mhufnage.data17_13TeV.00329542.physics_MinBias.daq.RAW._dataset10_v00_2023_EXT0,user.mhufnage.data17_13TeV.00329542.physics_MinBias.daq.RAW._dataset11_v00_2023_EXT0,user.mhufnage.data17_13TeV.00329542.physics_MinBias.daq.RAW._dataset12_v00_2023_EXT0,user.mhufnage.data17_13TeV.00329542.physics_MinBias.daq.RAW._dataset13_v00_2023_EXT0,      --outDS=user.mhufnage.data17_13TeV_dumpedEventReader_TagAndProbe_v01      --disableAutoRetry      --useAthenaPackages      --outputs="*.root" --nGBPerJob=MAX      --cmtConfig=x86_64-centos7-gcc8-opt --useHomeDir --gluePackages EventReader --bexec "make" --extFile /build/x86_64-centos7-gcc8-opt/lib/libEventReader.so,build/x86_64-centos7-gcc8-opt/lib/libEventReader.so.dbg,/build/EventReader/CMakeFiles/EventReader.dir/Root/EventReaderAlg.cxx.o,/build/EventReader/CMakeFiles/EventReader.dir/Root/components/EventReader_entries.cxx.o,/build/EventReader/CMakeFiles/EventReader.dir/Root/components/EventReader_load.cxx.o 

## Datasets Used: (ESD Files)
# user.mhufnage.data17_13TeV.00329542.physics_MinBias.daq.RAW._dataset01_v02_2023_EXT0,user.mhufnage.data17_13TeV.00329542.physics_MinBias.daq.RAW._dataset02_v02_2023_EXT0,user.mhufnage.data17_13TeV.00329542.physics_MinBias.daq.RAW._dataset03_v02_2023_EXT0,user.mhufnage.data17_13TeV.00329542.physics_MinBias.daq.RAW._dataset04_v02_2023_EXT0,user.mhufnage.data17_13TeV.00329542.physics_MinBias.daq.RAW._dataset05_v01_2023_EXT0,user.mhufnage.data17_13TeV.00329542.physics_MinBias.daq.RAW._dataset06_v02_2023_EXT0,user.mhufnage.data17_13TeV.00329542.physics_MinBias.daq.RAW._dataset07_v02_2023_EXT0,user.mhufnage.data17_13TeV.00329542.physics_MinBias.daq.RAW._dataset08_v01_2023_EXT0,user.mhufnage.data17_13TeV.00329542.physics_MinBias.daq.RAW._dataset09_v00_2023_EXT0,user.mhufnage.data17_13TeV.00329542.physics_MinBias.daq.RAW._dataset10_v00_2023_EXT0,user.mhufnage.data17_13TeV.00329542.physics_MinBias.daq.RAW._dataset11_v00_2023_EXT0,user.mhufnage.data17_13TeV.00329542.physics_MinBias.daq.RAW._dataset12_v00_2023_EXT0,user.mhufnage.data17_13TeV.00329542.physics_MinBias.daq.RAW._dataset13_v00_2023_EXT0