#!/bin/sh
# Get OFC from the Tile Cool Database

# ReadOfcFromCool.py -h
# Usage:  /cvmfs/atlas.cern.ch/repo/sw/software/22.0/Athena/22.0.70/InstallArea/x86_64-centos7-gcc11-opt/bin/ReadOfcFromCool.py  [OPTION] ... 
# Dumps the TileCal OFC values various schemas / folders / tags

# -h, --help      shows this help
# -f, --folder=   specify status folder to use CIS, LAS, PHY 
# -r, --run=      specify run  number, by default uses latest iov
# -l, --lumi=     specify lumi block number, default is 0
# -p, --ros=      specify partition (ros number), default is 1
# -d, --drawer=   specify drawer number, default is 0
# -c, --channel=  specify channel number, default is 0
# -g, -a, --adc=  specify gain (adc number), default is 0
# -i, --field=    specify field number, default is 0
# -s, --schema=   specify schema to use, like 'COOLONL_TILE/CONDBR2' or 'sqlite://;schema=tileSqlite.db;dbname=CONDBR2'

# ReadOfcFromCool.py -i 0
# ReadOfcFromCool.py -i 1
# ReadOfcFromCool.py -i 2
# ReadOfcFromCool.py -i 3
# ReadOfcFromCool.py -i 4

# we have five coeff sets in total: 
# 0: a, 1: b, 2: c, 3: g, 4: dg, all of them in a range from -100 to +100 ns
# OBS.: it might change when the weights will be implemented using the noise corr. matrix

# setupATLAS
# asetup Athena,22.0.70

for i in {0..4}
    do
        echo "ReadOfcFromCool.py -i $i"
        ReadOfcFromCool.py -i $i > TileOFC_$i.log 2>&1
    done


