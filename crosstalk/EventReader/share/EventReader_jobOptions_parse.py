###############################################################
#
# Job options file
#
#==============================================================
#--------------------------------------------------------------
# ATLAS default Application Configuration options
#--------------------------------------------------------------
import AthenaCommon.AtlasUnixStandardJob

#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# doFwd = False
# doMC = True
# dumpCells = False
# doPhoton = False
from AthenaCommon.AppMgr import ToolSvc
from AthenaCommon.AthenaCommonFlags import jobproperties as jps

# Full job is a list of algorithms
from AthenaCommon.AlgSequence import AlgSequence
job = AlgSequence()

from EventReader.EventReaderConf import EventReaderAlg

#### Geometry relation flags
from AthenaCommon.GlobalFlags import jobproperties
from AthenaCommon.DetFlags import DetFlags
from AthenaCommon.GlobalFlags import globalflags

Geometry = "ATLAS-R2-2016-01-00-01"
globalflags.DetGeo.set_Value_and_Lock('atlas')
DetFlags.detdescr.all_setOn()
DetFlags.Forward_setOff()
DetFlags.ID_setOff()

jobproperties.Global.DetDescrVersion = Geometry

    # We need the following two here to properly have
    # Geometry
from AtlasGeoModel import GeoModelInit
from AtlasGeoModel import SetGeometryVersion
include("CaloDetMgrDetDescrCnv/CaloDetMgrDetDescrCnv_joboptions.py")
include("LArDetDescr/LArDetDescr_joboptions.py")
#####

# Cabling map acess (LAr)
from LArCabling.LArCablingAccess import LArOnOffIdMapping
LArOnOffIdMapping()

job += EventReaderAlg( "EventReader" )

job.EventReader.clusterName     = "CaloCalTopoClusters"
job.EventReader.jetName         = "AntiKt4EMPFlowJets"
job.EventReader.roiRadius       = 0.2
job.EventReader.tileDigName     = "TileDigitsCnt"
job.EventReader.larDigName      = 'FREE'#"LArDigitContainer_MC"
job.EventReader.tileRawName     = "TileRawChannelCnt"
job.EventReader.larRawName      = "LArRawChannels"
job.EventReader.doTile          = True
job.EventReader.noBadCells      = True
job.EventReader.doLAr           = True
job.EventReader.printCellsClus  = False
job.EventReader.printCellsJet   = False
job.EventReader.testCode        = False
# job.EventReader.OuputLevel = INFO
job.EventReader.isMC            = False  # set to True in case of MC sample.

from AthenaCommon.AppMgr import ServiceMgr
import AthenaPoolCnvSvc.ReadAthenaPool

from os import system

if not 'FileName' in dir():
    FileName = '/eos/user/m/mhufnage/scripts_lxplus/Reco/ALP_reco/reco_HITStoESD_evenMoreEvents/ESD_pi0_000001.pool.root'
if not 'OutputFileName' in dir():
    OutputFileName='ntuple.root'
if not 'isMC' in dir():
    job.EventReader.isMC    = False
    globalflags.DataSource.set_Value_and_Lock('data') 

# testFile = "/eos/user/m/mhufnage/scripts_lxplus/Reco/ALP_reco/reco_HITStoESD/ESD_pi0.pool.root"

# testFile = "/eos/user/m/mhufnage/scripts_lxplus/Reco/ALP_reco/reco_RDO_LarDigits/ESD_fromRDO-HITS_PI0_001783.pool.root"
# testFile = "/eos/user/m/mhufnage/scripts_lxplus/Reco/ALP_reco/reco_HITStoESD_AMItags/ESD_pi0.pool.root" #AMI TAG reco
# testFile = "/eos/user/m/mhufnage/scripts_lxplus/Reco/ALP_reco/reco_HITStoESD_AMIdumpCells/ESD_pi0.pool.root"
# inputFile = "/eos/user/m/mhufnage/scripts_lxplus/Reco/ALP_reco/reco_HITStoESD_moreEvents/ESD_pi0.pool.root"

# inputFile = '/eos/user/m/mhufnage/scripts_lxplus/Reco/ALP_reco/reco_HITStoESD_evenMoreEvents/ESD_pi0_002450.pool.root'
# inputFile = '/eos/user/m/mhufnage/scripts_lxplus/Reco/ALP_reco/reco_HITStoESD_evenMoreEvents/ESD_pi0_000001.pool.root'
ServiceMgr.EventSelector.InputCollections = [ FileName ]
ServiceMgr += CfgMgr.THistSvc()

# Create output file
hsvc = ServiceMgr.THistSvc
# jps.AthenaCommonFlags.HistOutputs = ["MYSTREAM:myfile.root"]
hsvc.Output += [ "rec DATAFILE=\'{}\' OPT='RECREATE'" .format(OutputFileName) ]
# hsvc.Output += [ "recHist DATAFILE='hist.root' OPT='RECREATE'" ]
theApp.EvtMax = -1

MessageSvc.defaultLimit = 9999999  # all messages