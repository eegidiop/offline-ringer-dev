###############################################################
#
# Job options file
# - User options for grid usage.
#==============================================================
import argparse
from glob import glob
import sys

runLocal                = True  # if false, it will follow the grid selection.
inputDataPath           = '/eos/user/m/mhufnage/scripts_lxplus/Reco/ALP_reco/MC_Zee_HITStoESD/ESD*.root'
# inputDataPath           = '/eos/user/m/mhufnage/scripts_lxplus/Reco/ALP_reco/lowPileUpRun_RAWtoESD/ESD*.root'
isMC                    = True
numOfEvents             = 100
noBadCh                 = False  # If True, skip the cells tagged as badCells/channels.
dumpOnlySingleElectrons = True  # If false, dump electrons from Z->ee. If True, dont care about tag and Probe selection.

#--------------------------------------------------------------
# ATLAS default Application Configuration options
#--------------------------------------------------------------
import AthenaCommon.AtlasUnixStandardJob
from AthenaCommon.AppMgr import ServiceMgr
import AthenaPoolCnvSvc.ReadAthenaPool
from AthenaCommon.AppMgr import ToolSvc
from AthenaCommon.AthenaCommonFlags import jobproperties as jps


if runLocal:
    # ServiceMgr.EventSelector.InputCollections = inputFile

    inputFiles = []
    for file in glob(inputDataPath):
        inputFiles.append(file)
    # print(inputFile)
    # sys.exit()

    from AthenaCommon.AthenaCommonFlags import athenaCommonFlags
    athenaCommonFlags.FilesInput = inputFiles

    from AthenaConfiguration.AllConfigFlags import ConfigFlags
    ConfigFlags.Input.Files = athenaCommonFlags.FilesInput()


ServiceMgr += CfgMgr.THistSvc()

# svcMgr.EventSelector.InputCollections = PoolESDInput
# from AthenaCommon.AthenaCommonFlags import athenaCommonFlags

# if doHVCorr: # it is really needed?
#     from LArCellRec.LArCellRecConf import LArCellContHVCorrTool
    # theLArCellHVCorrTool = LArCellContHVCorrTool()

# #---------------Code Test Area-------------------------------#
# for datasetFilename in ConfigFlags.Input.Files:
#     print("Filename: ",datasetFilename)
#     print("Lumiblocks: ",ConfigFlags.Input.LumiBlockNumber)
# #-------------------------------------------------------------------#

#------------------------------------------------------------------------------------------------------
#                                   Create output file 
#------------------------------------------------------------------------------------------------------

hsvc = ServiceMgr.THistSvc
hsvc.Output += [ "rec DATAFILE='MC_Zee_test.root' OPT='RECREATE'" ]
theApp.EvtMax = numOfEvents

MessageSvc.defaultLimit = 9999999  # all messages

#------------------------------------------------------------------------------------------------------
#                           Detector Geometry includes and Job_options
#------------------------------------------------------------------------------------------------------

# Full job is a list of algorithms
from AthenaCommon.AlgSequence import AlgSequence
job = AlgSequence()

from EventReader.EventReaderConf import EventReaderAlg

#### Geometry relation flags
from AthenaCommon.GlobalFlags import jobproperties
from AthenaCommon.DetFlags import DetFlags
from AthenaCommon.GlobalFlags import globalflags

Geometry = "ATLAS-R2-2016-01-00-01"
globalflags.DetGeo.set_Value_and_Lock('atlas')

if (isMC):
    globalflags.DataSource.set_Value_and_Lock('geant4')
else:
    globalflags.DataSource.set_Value_and_Lock('data')


DetFlags.detdescr.all_setOn()
DetFlags.Forward_setOff()
DetFlags.ID_setOff()

jobproperties.Global.DetDescrVersion = Geometry

    # We need the following two here to properly have
    # Geometry
from AtlasGeoModel import GeoModelInit
from AtlasGeoModel import SetGeometryVersion

##### Conditions and Geometry Includes
include( "CaloConditions/CaloConditions_jobOptions.py" )
include( "CaloIdCnv/CaloIdCnv_joboptions.py" )
include( "TileIdCnv/TileIdCnv_jobOptions.py" )
include( "TileConditions/TileConditions_jobOptions.py" )
include( "LArDetDescr/LArDetDescr_joboptions.py" )
if (isMC):
    # include( "LarConditionsCommon/LArConditionsCommon_MC_jobOptions.py")
    print('include here the folder....')
else: # Data
    include( "LArConditionsCommon/LArConditionsCommon_comm_jobOptions.py")
    # include("CaloDetMgrDetDescrCnv/CaloDetMgrDetDescrCnv_joboptions.py") # obsolete?? yes

#------------------------------------------------------------------------------------------------------
#                           Calibration constants and Cabling access
#------------------------------------------------------------------------------------------------------

from CaloTools.CaloNoiseCondAlg import CaloNoiseCondAlg
CaloNoiseCondAlg ('totalNoise')
CaloNoiseCondAlg ('electronicNoise')
CaloNoiseCondAlg ('pileupNoise')

# Online adc to MeV
from LArRecUtils.LArADC2MeVCondAlgDefault import LArADC2MeVCondAlgDefault
LArADC2MeVCondAlgDefault()

# Per-cell Pile-up offset correction (Offline)
from CaloRec.CaloBCIDCoeffsCondAlgDefault import CaloBCIDCoeffsCondAlgDefault
CaloBCIDCoeffsCondAlgDefault()

if globalflags.DataSource()=='data':
    from LumiBlockComps.LuminosityCondAlgDefault import LuminosityCondAlgDefault
    LuminosityCondAlgDefault()
else: # MC
    from LumiBlockComps.BunchCrossingCondAlgDefault import BunchCrossingCondAlgDefault
    BunchCrossingCondAlgDefault()

# from LArCalibUtils.LArHVScaleConfig import LArHVScaleCfg

# Cabling map acess (LAr)
from LArCabling.LArCablingAccess import LArOnOffIdMapping
LArOnOffIdMapping()

#------------------------------------------------------------------------------------------------------
#                                     Algorithm setup and execution
#------------------------------------------------------------------------------------------------------
job += EventReaderAlg( "EventReader" )

job.EventReader.clusterName                 = "CaloCalTopoClusters"#"LArClusterEM7_11Nocorr"#"LArClusterEM7_11Nocorr"#"egammaClusters"#
job.EventReader.jetName                     = "AntiKt4EMPFlowJets"
job.EventReader.roiRadius                   = 0.2
job.EventReader.tileDigName                 = "TileDigitsCnt"
if isMC:        
    job.EventReader.larDigName              = "LArDigitContainer_MC"
else:       
    job.EventReader.larDigName              = 'FREE' #'LArDigitContainer_EMClust'#'LArDigitContainer_Thinned' #
job.EventReader.tileRawName                 = "TileRawChannelCnt"
job.EventReader.larRawName                  = "LArRawChannels"

# -- Dump variables Selection -- 
# job.EventReader.doTile                      = True    # must be implemented
# job.EventReader.doLAr                       = True    # must be implemented
job.EventReader.doClusterDump               = False # Dump only a cluster container. (override the electron cluster)
job.EventReader.doJetRoiDump                = False # Perform a jet roi cell dump based on Jet Container name pr_jetName + Build Rings in JetROI
job.EventReader.doTruthEventDump            = False # ump the Truth Event variables.
job.EventReader.doPhotonDump                = False # Perform a photon particle dump based on offline Photons Container.
job.EventReader.doTruthPartDump             = False # Perform a truth particle dump.

job.EventReader.noBadCells                  = noBadCh  # If True, skip the cells tagged as badCells/channels.

job.EventReader.printCellsClus              = True #False # Debugging
job.EventReader.printCellsJet               = False # Debugging
job.EventReader.testCode                    = False # Debugging

# Electrons and crosstalk studies
job.EventReader.doTagAndProbe               = True  # select by tag and probe method, electron pairs (start the chain of selection: track + T&P)
job.EventReader.doElecSelectByTrackOnly     = dumpOnlySingleElectrons  # select only single electrons which pass track criteria (only track)
job.EventReader.getAssociatedTopoCluster    = True  # Get the topo cluster associated to a super cluster, which was linked to an Electron
job.EventReader.getLArCalibConstants        = True  # Get the LAr calorimeter calibration constants, related to cells energy and time (online and offline).
job.EventReader.etMinProbe                  = 15    # Min electron Pt value for Zee probe selection loose (GeV).
job.EventReader.etMinTag                    = 15    # Min Et value for the electrons in Zee tag selection (GeV).
job.EventReader.etMaxTag                    = 180   # Max Et value for the electrons in Zee tag selection (GeV).
job.EventReader.minZeeMassTP                = 66    # Minimum value of Zee mass for checking the TP pairs (GeV).
job.EventReader.maxZeeMassTP                = 116   # Maximum value of Zee mass for checking the TP pairs (GeV).
# 
job.EventReader.OutputLevel                 = DEBUG##INFO # # 
job.EventReader.isMC                        = isMC  # set to True in case of MC sample.

#------------------------------------------------------------------------------------------------------
#                                      Condition Database Access
#------------------------------------------------------------------------------------------------------
# based on: https://acode-browser1.usatlas.bnl.gov/lxr/source/athena/LArCalorimeter/LArROD/python/LArRawChannelBuilderDefault.py
#           https://acode-browser1.usatlas.bnl.gov/lxr/source/athena/Calorimeter/CaloRec/python/CaloCellGetter.py#0519
# Helper reading:
#           https://twiki.cern.ch/twiki/bin/view/Main/AthenaCodeSnippets#A_lesson_in_conditions_databases

obj = "AthenaAttributeList"
dbOnline    = 'LAR_ONL'
dbOffline   = 'LAR_OFL'

if globalflags.DataSource() == 'data':
    from IOVDbSvc.CondDB import conddb
    if conddb.GetInstance() == 'COMP200':  # Run1
        fldThr='/LAR/Configuration/DSPThreshold/Thresholds'
        # job.EventReader.Run1DSPThresholdsKey='LArDSPThresholds' ## NOT IMPLEMENTED FOR RUN1 DATA  ! 
        obj='LArDSPThresholdsComplete'
    else: # Run2
        fldThr="/LAR/Configuration/DSPThresholdFlat/Thresholds"
        fldOflEneResc="/LAR/CellCorrOfl/EnergyCorr"
        job.EventReader.Run2DSPThresholdsKey=fldThr
        job.EventReader.OflEneRescalerKey=fldOflEneResc        
        conddb.addFolder (dbOffline, fldOflEneResc, className=obj)
    conddb.addFolder (dbOnline, fldThr, className=obj)

else: #-- MC ---#
    from AtlasGeoModel.CommonGMJobProperties import CommonGeometryFlags
    from IOVDbSvc.CondDB import conddb
    if CommonGeometryFlags.Run() == "RUN1": # back to flat threshold (NOT IMPLEMENTED)
        job.EventReader.useDB = False
        job.EventReader.Run2DSPThresholdsKey=''
    else: # Run2
        # fldThr="/LAR/NoiseOfl/DSPThresholds"
        fldThr=""
        fldOflEneResc=""
        job.EventReader.Run2DSPThresholdsKey=fldThr
        job.EventReader.OflEneRescalerKey=fldOflEneResc
        # conddb.addFolder (dbOffline, fldOflEneResc, className=obj)
        # db ='LAR_OFL'

        ## Shape
        from LArRecUtils.LArOFCCondAlgDefault import LArOFCCondAlgDefault
        from LArRecUtils.LArAutoCorrTotalCondAlgDefault import  LArAutoCorrTotalCondAlgDefault
        from LArRecUtils.LArADC2MeVCondAlgDefault import LArADC2MeVCondAlgDefault
        LArADC2MeVCondAlgDefault()
        LArAutoCorrTotalCondAlgDefault()
        LArOFCCondAlgDefault()
        conddb.addFolder("LAR_OFL","/LAR/ElecCalibMC/LArPileupAverage",className="LArMinBiasAverageMC")
        conddb.addFolder("LAR_OFL","/LAR/ElecCalibMC/Shape",className="LArShape32MC") #LArMC32Sym

        # job.EventReader.ShapeKey    = 'LArShape32'

        # ShapeKey = 'LArShapeSym'
    # conddb.addFolder(dbOnline, fldThr, className=obj)
    # conddb.addFolder(dbOffline, fldOflEneResc, className=obj)

# if job.EventReader.isMC==False:
#     globalflags.DataSource.set_Value_and_Lock('data') 
