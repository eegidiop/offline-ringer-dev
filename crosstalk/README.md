# **EventReader Dumper package**

The main purpose and idea of this package, is to provide access to physics objects, like Jets, Electrons, Photons, and their associated calorimeter level data such as clusters, cells and calibration constants. For being properly used, it obeys the following methodology:
- Take the physics stream of interest (HITS if MC, or RAW if Data);
- Apply the transformation until the ESD, forwarding some containers (RawChannel and Digits);
- Run the EventReader dumper algorithm on those, generating a NTuple file, that is organized in such a way, that the physics objects are linked to their calorimeteric variables.

Those steps are detailed below, using as an example, the Zee for crosstalk studies.

## **1. Download the data or MC stream**
- 1.1. Download MC Zee dataset:
    ```
    rucio list-dids mc16_13TeV:mc16_13TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.simul.HITS.e3601_e5984_s3240
    rucio list-dataset-replicas mc16_13TeV:mc16_13TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.simul.HITS.e3601_e5984_s3240
    ```
    * If it is stored in tape, request it and download:
    ```
    rucio download mc16_13TeV:mc16_13TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.simul.HITS.e3601_e5984_s3240
    ```

- 1.2. Download run data stream:

    Dataset previous usage in [ATLAS e/gamma workshop 2017](https://indico.cern.ch/event/649891/timetable/), in [this presentation](https://indico.cern.ch/event/649891/contributions/2750915/attachments/1554469/2444022/2017_11_08-2.pdf). 

* Runs 329542 and 331020: 
    * [Twiki link for 2017 reprocessing](https://twiki.cern.ch/twiki/bin/view/Atlas/Special2017Reprocessing)
    * [Good runs list with low pile-up 2017 (GRL)](https://atlas-tagservices.cern.ch/tagservices/RunBrowser/runBrowserReport/COMA_GRL.php?GRLAction=GRLReport&gpath=grlgen/Minbias_2017&gfile=data17_13TeV.periodAllYear_DetStatus-v96-pro21-11_Unknown_PHYS_StandardModel_MinimuBias2010.xml)

* Basic Rucio commands:
    ```
    rucio list-dids data17_13TeV:data17_13TeV.00329542.physics_MinBias.*
    rucio list-dataset-replicas data17_13TeV:data17_13TeV.00329542.physics_MinBias.daq.RAW
    rucio download data17_13TeV:data17_13TeV.00329542.physics_MinBias.daq.RAW --nrandom=1
    rucio list-files data17_13TeV:data17_13TeV.00329542.physics_MinBias.daq.RAW
    ```
* Verify if dataset is in tape:

    ```
    rucio list-dataset-replicas data17_13TeV:data17_13TeV.00329542.physics_MinBias.daq.RAW
    ```
    ```
        DATASET: data17_13TeV:data17_13TeV.00329542.physics_MinBias.daq.RAW
        +----------------------+---------+---------+
        | RSE                  |   FOUND |   TOTAL |
        |----------------------+---------+---------|
        | CERN-PROD_RAW        |    3350 |    3350 |
        | TRIUMF-LCG2_DATATAPE |    3350 |    3350 |
        +----------------------+---------+---------+
    ```
    ```
    rucio list-rse-attributes CERN-PROD_RAW
    ```
    ```
        CERN-PROD_RAW:           True
        archive_timeout:         86400
        bb8-enabled:             False
        cloud:                   CERN
        datapolicyanalysis:      True
        datapolicynucleus:       True
        datapolicyt0disk:        False
        datapolicyt0tape:        False
        datapolicyt0taskoutput:  False
        freespace:               1399
        fts:                     https://fts3-atlas.cern.ch:8446
        istape:                  True
        naming_convention:       T0
        site:                    CERN-PROD
        spacetoken:              ATLASDATATAPE
        staging_buffer:          CERN-PROD_TAPE-STAGING
        tier:                    0
        type:                    SPECIAL
    ```

* If the data is in tape (**istape: True**), request via [Rucio-UI](https://rucio-ui.cern.ch/r2d2/request). The ```user.mhufnage.data17_13TeV.00329542.physics_MinBias.daq.RAW_der1655905167``` is the requested data, stored into CERN-PROD_SCRATCHDISK for temporary access. It can be verified by the rucio command below:
    ```
    rucio list-dataset-replicas user.mhufnage.data17_13TeV.00329542.physics_MinBias.daq.RAW_der1655905167
    +-----------------------+---------+---------+
    | RSE                   |   FOUND |   TOTAL |
    |-----------------------+---------+---------|
    | CERN-PROD_SCRATCHDISK |       0 |      10 |
    +-----------------------+---------+---------+
    rucio list-files user.mhufnage.data17_13TeV.00329542.physics_MinBias.daq.RAW_der1655905167
    ```
<!-- * After downloading it, apply: 

    ```checkFile.py ESD.pool.root``` -->

## **2. Transformation (via Reco_tf.py)**
- 2.1. Open the ```share/reco_grid_RAWtoESD.sh``` support file. There are many examples (Trial and error too) of transformation commands for grid and lxplus.

- 2.2. HITS to ESD (MC) simplest reconstruction (Local):

    ```
    setupATLAS
    asetup Athena,22.0.44
    ```
    ```
    Reco_tf.py --maxEvents=-1 --athenaopts=' --threads=1 --imf --perfmon' --AddCaloDigi True --inputHitsFile=HITS.12832049._010826.pool.root.1 --outputRDOFile=RDO.pool.root > reco.log 2>&1
    Reco_tf.py --athenaopts=' --threads=1 --imf --perfmon' --AddCaloDigi=True --inputRDOFile=RDO.pool.root --outputESDFile=ESD.pool.root --postExec='StreamESD.ItemList+=["TileDigitsContainer#TileDigitsCnt", "LArDigitContainer#FREE", "LArRawChannelContainer#LArRawChannels", "TileRawChannelContainer#TileRawChannelCnt"]'
    ```

<!-- ## **3. Bytestream (RAW) data to ESD (Data)** -->

- 2.3. Bytestream (RAW) data to ESD example (Grid):
    ```
    setupATLAS
    asetup Athena,21.0.30
    lsetup panda
    ```
    ```
    pathena --trf "Reco_tf.py  --inputBSFile=%IN --outputESDFile=%OUT.ESD.pool.root --AMIConfig=r9738 --AMITag=r9738 --maxEvents=3500 --AddCaloDigi=True --postExec='RAWtoESD:StreamESD.ItemList+=[\"TileDigitsContainer#TileDigitsCnt\",\"LArDigitContainer#FREE\",\"LArRawChannelContainer#LArRawChannels\",\"TileRawChannelContainer#TileRawChannelCnt\"]' " --inDS=user.mhufnage:user.mhufnage.data17_13TeV.00329542.MinBias.daq.RAW_dataset08 --outDS=user.mhufnage.data17_13TeV.00329542.physics_MinBias.daq.RAW._dataset08_v01_2023 --noBuild --nGBPerJob 5 --nEventsPerFile 3500 --nCore 4
    ```
    ```
    rucio list-dids user.mhufnage:*
    ```
    
    **Side note:** The transformation using MinBias Stream files were crashing because of the size of ESD files. It was 'solved' reducing the number of events per file (down to 3500), but can be optimized. It decresed the number of total events from ~13 million, to ~8 million.
    

<!-- - 2.1. Download DRAW_ZEE
   

    ```
    rucio list-dataset-replicas "data12_8TeV:data12_8TeV.00200913.physics_Egamma.merge.DRAW_ZEE.f437_m716"
        DATASET: data12_8TeV:data12_8TeV.00200913.physics_Egamma.merge.DRAW_ZEE.f437_m716
        +-----------------+---------+---------+
        | RSE             |   FOUND |   TOTAL |
        |-----------------+---------+---------|
        | CERN-PROD_TZERO |       2 |       2 |
        +-----------------+---------+---------+
    ```
    The dataset **data12_8TeV.00200913.physics_Egamma.merge.DRAW_ZEE.f431_m716** is stored on tape.
    ```    
    rucio list-dids "data12_8TeV:*200913*" --filter type=dataset,datatype=DRAW_ZEE
    ``` -->
## **3. Configuring the Dumper**
Download it and build:
```
mkdir build && cd build/
setupATLAS && asetup Athena,22.0.44 && make -j2
source x86*/setup.sh
cd .. && mkdir run && cd run/
```
To run the ESD dumper, the ```EventReader_crosstalk_jobOptions``` file must be configured (you can create your own by modifying it). There are many options of data to be dumped via flags.
```
athena.py ../EventReader/share/EventReader_crosstalk_jobOptions.py > reader.log 2>&1
```
To run it inside the Grid, it can be supported by the ```share/RunEventReaderOnGrid.py``` file, to printout the commands. Here, the **build** directory must be at the same level of **prun** command being executed.
```
python EventReader/share/RunEventReaderOnGrid.py --inDS user.TheUserName.inputESD --outDS user.TheUserName.outputDataset.root
```
Output:
```
prun --exec        "source build/x86_64-centos7-gcc8-opt/setup.sh;athena.py EventReader/share/EventReader_crosstalk_gridJobOptions.py --filesInput='%IN';"      --inDS=user.TheUserName.inputESD      --outDS=user.TheUserName.outputDataset.root      --disableAutoRetry      --outputs="*.root"      --useAthenaPackages      --nGBPerJob=MAX      --cmtConfig=x86_64-centos7-gcc8-opt      --useHomeDir      --gluePackages EventReader      --bexec "make"      --extFile /build/x86_64-centos7-gcc8-opt/lib/libEventReader.so,build/x86_64-centos7-gcc8-opt/lib/libEventReader.so.dbg,/build/EventReader/CMakeFiles/EventReader.dir/Root/EventReaderAlg.cxx.o,/build/EventReader/CMakeFiles/EventReader.dir/Root/components/EventReader_entries.cxx.o,/build/EventReader/CMakeFiles/EventReader.dir/Root/components/EventReader_load.cxx.o
```

<!-- ## **4. Python scripts (EventReader/share/)** -->
<!-- - 4.1. Validation plots (validPlots.py)
    ```
    python validPlots.py --help
        usage: validPlots.py [-h] [--fileAlias FILEALIAS] [--getNoise GETNOISE]
                            [--events EVENTS] [--outputFilesPath OUTPUTFILESPATH]
                            [--inputFilesPath INPUTFILESPATH]

        Script to read the dumped file from EventReader, and generate the validation
        plots as TH1F in *.root files.

        optional arguments:
        -h, --help            show this help message and exit
        --fileAlias FILEALIAS
                                name alias to generated files.
        --getNoise GETNOISE   get the histogram and dict of noisy cells around the
                                Jet ROI.
        --events EVENTS       amount of events to be processed. Default is all the
                                events pointed by the file directory.
        --outputFilesPath OUTPUTFILESPATH
                                path for the output *.root files. It may create
                                folders in this dir.
        --inputFilesPath INPUTFILESPATH
                                path for the file(s). It accepts the glob (*)
                                notation.
    ```
    - There are two main commands:
        * Get Noise parameters per layer.
            ```
            python validPlots.py \
            --getNoise=True \
            --inputFilesPath='/path/to/data/ESD_pi0_*.root' \ 
            --outputFilesPath='/path/to/output/run/singlePi0_50kEvts'
            ```
            * The _--getNoise_ parse will override the general plots generation, and will only loop over the cells most distant in the JetRoi of the dumped data, saving the _CaloCell Energy_ and the _RawChannel Energy_ in an output **'noiseCellJetROI.root'**, and their _mean_ and $\sigma$ in a dictionary **caloNoiseDict.json**.
            * All of these files will be saved in the output directory, parsed _outputFilesPath_.
            * _--events_ is equal to -1 by default (all events)

        * Generate the histograms for the dumped data with cuts (based on the noise dictionary generated earlier).
            * This script will load the generated file with the layer noise information, then produce all the plots with their own version with the cut at $e_{cut} = 2 *\sigma$ (default). A log file will be created for each script running, appending the outputs at the same file ****
            * The _--fileAlias_ parse is just an alias added at the beginning of the generated files and the LOG.
            * 
            ```
            python validPlots.py --inputFilesPath='/path/to/data/ESD_*.root' \ 
            --outputFilesPath = '/path/to/output/run/singlePi0_50kEvts' \
            --fileAlias='testSinglePi0_' \
            --events=-1

            
            python validPlots.py --getNoise=True --inputFilesPath='/eos/user/m/mhufnage/scripts_lxplus/Grid/workAthena/ALPPackages/run/ntuples/ESD_pi0_*.root'  --outputFilesPath='/eos/user/m/mhufnage/scripts_lxplus/Grid/workAthena/ALPPackages/run/testDigits'  --fileAlias='testSinglePi0_' --events=100

            python validPlots.py  --inputFilesPath='/eos/user/m/mhufnage/scripts_lxplus/Grid/workAthena/ALPPackages/run/ntuples/ESD_pi0_*.root'  --outputFilesPath='/eos/user/m/mhufnage/scripts_lxplus/Grid/workAthena/ALPPackages/run/testDigits'  --fileAlias='testSinglePi0_' --events=100
            ```
    - Examples for batch tests:
        ```
        python validPlots.py --inputFilesPath='/eos/user/m/mhufnage/scripts_lxplus/Grid/workAthena/ALPPackages/run/ntuples/ESD_pi0_*.root' --outputFilesPath='/eos/user/m/mhufnage/scripts_lxplus/Grid/workAthena/ALPPackages/run/singlePi0_50kEvts' --events=10000 --fileAlias='3CellPerBin_' --nCellPerBin=3
       
        python validPlots.py --inputFilesPath='/eos/user/m/mhufnage/scripts_lxplus/Grid/workAthena/ALPPackages/run/ntuples/ESD_pi0_*.root' --outputFilesPath='/eos/user/m/mhufnage/scripts_lxplus/Grid/workAthena/ALPPackages/run/singlePi0_50kEvts' --events=1 --fileAlias='test_'
        ```
        ```
        python validPlots.py --inputFilesPath='/eos/user/m/mhufnage/scripts_lxplus/Grid/workAthena/ALPPackages/run/ntuples/test_MCZee.root' --outputFilesPath='/eos/user/m/mhufnage/scripts_lxplus/Grid/workAthena/ALPPackages/run/ZeeHist' --fileAlias='mc_' --xTalkStudies=True --events=100
        ```

- 4.2. Reading histograms (readHisto.py)
    ```
    python readHisto.py --help
    usage: readHisto.py [-h] [--fileAlias FILEALIAS]
                        [--outputFilesPath OUTPUTFILESPATH]
                        [--inputFilesPath INPUTFILESPATH]

    Script to read the histograms generated by validPlots.py, in *.root files.

    optional arguments:
    -h, --help            show this help message and exit
    --fileAlias FILEALIAS
                            name alias to generated *.root files. (
                            <fileAlias>jetAnalysis.root )
    --outputFilesPath OUTPUTFILESPATH
                            path for the automatic generated output folders with
                            histograms. If the folder doesnt exist, it will create it.
    --inputFilesPath INPUTFILESPATH
                            path for the histogram file(s).
    ```
    * Exemple:
    ```
    python readHisto.py --inputFilesPath='/eos/user/m/mhufnage/scripts_lxplus/Grid/workAthena/ALPPackages/run/singlePi0_50kEvts/histograms/' --outputFilesPath='/eos/user/m/mhufnage/scripts_lxplus/Grid/workAthena/ALPPackages/run/singlePi0_50kEvts/plots/1CellPerBin/' --fileAlias='1CellPerBin_'
    ```

- 4.3. Batch validation plots and histograms (validPlotsBatch.sh)
     ```
    cd EventReader/share/
    # <update it with the desired commands>
    bash validPlotsBatch.sh
    ``` -->


<!-- ## **5. Sparse checkout Athena packages**
```
cd
cd private
git atlas init-workdir https://gitlab.cern.ch/atlas/athena.git
cd athena
git branch
# * master
git fetch upstream
git rebase upstream/master # atualiza Master local com ATHENA MASTER
git push origin master # atualiza o fork com o ATHENA MASTER

git checkout -b jetAddPtThresMin upstream/22.0 --no-track # cria nova branch no ATHENA 22.0 (sem atualizacao automatica)
git atlas addpkg JetRec

cmake ../athena/Projects/WorkDir/
source x86_64-centos7-gcc11-opt/setup.sh

``` -->

<!-- TileCal readout system
element     range       meaning
-------     -----       -------

ros         1 to 4      ReadOutSystem number ( 1,2 = pos/neg Barrel (side A/C)
                                                3,4 = pos/neg Ext.Barrel (side A/C) )
drawer      0 to 63     64 drawers (modules) in one cylinder (phi-slices)
channel     0 to 47     channel number in the drawer
adc         0 to 1      ADC number for the channel (0 = low gain, 1 = high gain)


Possible Gain values
    enum CaloGain {
        TILELOWLOW =-16 ,
        TILELOWHIGH =-15 ,
        TILEHIGHLOW  = -12,
        TILEHIGHHIGH = -11,
        TILEONELOW   =-4,
        TILEONEHIGH  =-3,
        INVALIDGAIN = -1, 
        LARHIGHGAIN = 0, 
        LARMEDIUMGAIN = 1,  
        LARLOWGAIN = 2,
        LARNGAIN =3,
        UNKNOWNGAIN=4}; -->