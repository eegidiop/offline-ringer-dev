// PhotonTruthMatchingTool includes
#include "RingerDumper/OfflineRingerDumperAlg.h"
#include <iostream>
#include <vector>

OfflineRingerDumperAlg::OfflineRingerDumperAlg( const std::string& name, ISvcLocator* pSvcLocator ) : AthAlgorithm( name, pSvcLocator ), m_ntsvc("THistSvc/THistSvc", name){
}

OfflineRingerDumperAlg::~OfflineRingerDumperAlg() {
}


StatusCode OfflineRingerDumperAlg::initialize() {
  ATH_MSG_INFO ("Initializing " << name() << "...");

  m_Tree = new TTree("events", "events");
    bookBranches(m_Tree);
    if (!m_ntsvc->regTree("/rec/", m_Tree).isSuccess()) {
      ATH_MSG_ERROR("could not register tree [events]");
      return StatusCode::FAILURE;
    }

  return StatusCode::SUCCESS;
}

void OfflineRingerDumperAlg::bookBranches(TTree *tree){
  tree->Branch("avgmu", &m_pileup);
 
  tree->Branch("offline_dummy_rings", &m_offlineDummy);
  tree->Branch("offline_std_rings", &m_offlineStdRings);
  tree->Branch("offline_asym_rings", &m_offlineAsymRings);
  tree->Branch("offline_strip_rings", &m_offlineStripsRings);

  tree->Branch("el_energy", &el_energy);
  tree->Branch("el_et", &el_et);
  tree->Branch("el_eta", &el_eta);
  tree->Branch("el_phi", &el_phi);
  tree->Branch("el_f1",&el_f1);
  tree->Branch("el_f3",&el_f3);
  tree->Branch("el_eratio",&el_eratio);
  tree->Branch("el_weta1",&el_weta1);
  tree->Branch("el_weta2",&el_weta2);
  tree->Branch("el_fracs1",&el_fracs1);
  tree->Branch("el_wtots1",&el_wtots1);
  tree->Branch("el_deltae",&el_deltae);
  tree->Branch("el_e2777",&el_e277);
  tree->Branch("el_reta",&el_reta);
  tree->Branch("el_rphi",&el_rphi);
  tree->Branch("el_rhad",&el_rhad);
  tree->Branch("el_rhad1",&el_rhad1);

  tree->Branch("el_truth_pt", &el_truth_pt);
  tree->Branch("el_truth_eta", &el_truth_eta);
  tree->Branch("el_truth_phi", &el_truth_phi);
  tree->Branch("el_truth_f1",&el_truth_f1);
  tree->Branch("el_truth_f3",&el_truth_f3);
  tree->Branch("el_truth_eratio",&el_truth_eratio);
  tree->Branch("el_truth_weta1",&el_truth_weta1);
  tree->Branch("el_truth_weta2",&el_truth_weta2);
  tree->Branch("el_truth_fracs1",&el_truth_fracs1);
  tree->Branch("el_truth_wtots1",&el_truth_wtots1);
  tree->Branch("el_truth_deltae",&el_truth_deltae);
  tree->Branch("el_truth_e277",&el_truth_e277);
  tree->Branch("el_truth_reta",&el_truth_reta);
  tree->Branch("el_truth_rphi",&el_truth_rphi);
  tree->Branch("el_truth_rhad",&el_truth_rhad);
  tree->Branch("el_truth_rhad1",&el_truth_rhad1);


  if (m_doMC){
    tree->Branch("mc_et", &mc_et);
    tree->Branch("mc_eta", &mc_eta);
    tree->Branch("mc_phi", &mc_phi);
    tree->Branch("mc_origin", &mc_origin);
    tree->Branch("mc_type", &mc_type);

    tree->Branch("mc_match_et", &mc_match_et);
    tree->Branch("mc_match_eta", &mc_match_eta);
    tree->Branch("mc_match_phi", &mc_match_phi);
    tree->Branch("mc_match_origin", &mc_match_origin);
    tree->Branch("mc_match_type", &mc_match_type);
  }
}

StatusCode OfflineRingerDumperAlg::execute() {  
  ATH_MSG_DEBUG ("Executing " << name() << "...");
  setFilterPassed(false); //optional: start with algorithm not passed
  if (!collectInfo()) ATH_MSG_DEBUG("Unable to collect info. Please check input file");
  m_Tree->Fill();
  return StatusCode::SUCCESS;
}

StatusCode OfflineRingerDumperAlg::collectInfo(){
  clear();
  if(!collectEventInfo())   ATH_MSG_DEBUG("Event information cannot be collected!");
  if(!collectRingsInfo())   ATH_MSG_DEBUG("Ringer information cannot be collected!");
  if(!collectElectronInfo()) ATH_MSG_DEBUG("Offline Electron information cannot be collected!");
  if(!collectOfflineInfo()) ATH_MSG_DEBUG("Offline information cannot be collected!");
  return StatusCode::SUCCESS;
}

void OfflineRingerDumperAlg::clear(){
  m_pileup = -999;
  
  m_offlineDummy->clear();
  m_offlineStdRings->clear();
  m_offlineAsymRings->clear();
  m_offlineStripsRings->clear();

  el_energy->clear();
  el_et->clear();
  el_eta->clear();
  el_phi->clear();
  el_f1->clear();
  el_f3->clear();
  el_eratio->clear();
  el_weta1->clear();
  el_weta2->clear();
  el_fracs1->clear();
  el_wtots1->clear();
  el_deltae->clear();
  el_e277->clear();
  el_reta->clear();
  el_rphi->clear();
  el_rhad->clear();
  el_rhad1->clear();

// offline truth
  el_truth_pt->clear();
  el_truth_eta->clear();
  el_truth_phi->clear();
  el_truth_f1->clear();
  el_truth_f3->clear();
  el_truth_eratio->clear();
  el_truth_weta1->clear();
  el_truth_weta2->clear();
  el_truth_fracs1->clear();
  el_truth_wtots1->clear();
  el_truth_e277->clear();
  el_truth_reta->clear();
  el_truth_rphi->clear();
  el_truth_deltae->clear();
  el_truth_rhad->clear();
  el_truth_rhad1->clear();

  if(m_doMC){
    mc_et->clear();
    mc_eta->clear();
    mc_phi->clear();
    mc_origin->clear();
    mc_type->clear();

    mc_match_et->clear();
    mc_match_eta->clear();
    mc_match_phi->clear();
    mc_match_origin->clear();
    mc_match_type->clear();
  }  
}

bool OfflineRingerDumperAlg::getCaloRings( const xAOD::Electron_v1 * el, std::vector<float> &ringsE ){

  if(!el) return false;
  ringsE.clear();
  auto m_ringsELReader = xAOD::getCaloRingsReader();
  // First, check if we can retrieve decoration: 
  const xAOD::CaloRingsLinks *caloRingsLinks(nullptr);
  try { 
    ATH_MSG_DEBUG("getCaloRingsReader->operator()(*el)");
    caloRingsLinks = &(m_ringsELReader(*el)); 
  } catch ( const std::exception &e) { 
    ATH_MSG_WARNING("Couldn't retrieve CaloRingsELVec. Reason: " << e.what()); 
    return false;
  } 
  if( caloRingsLinks ){
    if ( caloRingsLinks->empty() ){ 
      ATH_MSG_WARNING("Particle does not have CaloRings decoratorion.");
      return false;
    }
    const xAOD::CaloRings *clrings=nullptr;
    try {
      // For now, we are using only the first cluster 
      clrings = *(caloRingsLinks->at(0));
    } catch(const std::exception &e){
      ATH_MSG_WARNING("Couldn't retrieve CaloRings. Reason: " << e.what()); 
      return false;
    }
    if(clrings) {
      ATH_MSG_DEBUG("exportRingsTo...");
      clrings->exportRingsTo(ringsE);

    }else{
      ATH_MSG_WARNING("There is a problem when try to attack the rings vector using exportRigsTo() method.");
      return false;
    }

  }else{
    ATH_MSG_WARNING("CaloRingsLinks in a nullptr...");
    return false;
  }

  return true;
}

StatusCode OfflineRingerDumperAlg::collectElectronInfo(){

  const xAOD::ElectronContainer *offlineElectrons = nullptr;
  ATH_CHECK(evtStore()->retrieve(offlineElectrons, "Electrons"));
  ATH_MSG_DEBUG("Offline Electron Container Retrieved! That's good!");

  for (auto *electron : *offlineElectrons){
    
    if (!getCaloRings(electron, *m_offlineDummy)) continue;
    if (!collectOfflineSS(electron)) continue;
    el_energy->push_back ( electron->e()/1000 );
    el_et->push_back ( electron->e()/cosh(electron->eta())/1000);
    el_eta->push_back ( electron->eta() );
    el_phi->push_back ( electron->phi() );

  }
  ATH_MSG_DEBUG("Offline Electron Container Information Collected! That's good!");
  return StatusCode::SUCCESS;
}

StatusCode OfflineRingerDumperAlg::collectRingsInfo(){

  const xAOD::RingSetContainer *el_ring_container = nullptr;
  const xAOD::RingSetContainer *el_ring_asym_container = nullptr;
  const xAOD::RingSetContainer *el_ring_strips_container = nullptr;

  ATH_CHECK(evtStore()->retrieve(el_ring_container, "ElectronRingSets"));
  ATH_MSG_DEBUG("Rings - Std Container Retrieved! That's good!");

  ATH_CHECK(evtStore()->retrieve(el_ring_asym_container, "ElectronAsymRingSets"));
  ATH_MSG_DEBUG("Rings - Asym Container Retrieved! That's good!");

  ATH_CHECK(evtStore()->retrieve(el_ring_strips_container, "ElectronStripsRingSets"));
  ATH_MSG_DEBUG("Rings - Strips Container Retrieved! That's good!");

  std::vector<float> single_ring;
  
  for (const xAOD::RingSet_v1 *el_ring : *el_ring_container) {
    for (auto iter=el_ring->begin(); iter != el_ring->end(); iter++){
      single_ring.push_back(*iter);
    }
    if (single_ring.size() == 100){
      m_offlineStdRings->push_back(single_ring);
      single_ring.clear();
    }
  }

  ATH_MSG_DEBUG("Offline Standard Ringer Information Collected! That's good!");

  std::vector<float> single_asym_ring;
  
  for (const xAOD::RingSet_v1 *el_asym_ring : *el_ring_asym_container) {
    for (auto iter=el_asym_ring->begin(); iter != el_asym_ring->end(); iter++){
      single_asym_ring.push_back(*iter);
    }
    if (single_asym_ring.size() == 379){
      m_offlineAsymRings->push_back(single_asym_ring);
      single_asym_ring.clear();
    }
  }

  ATH_MSG_DEBUG("Offline Asym Ringer Information Collected! That's good!");

  std::vector<float> single_strips_ring;
  
  for (const xAOD::RingSet_v1 *el_strips_ring : *el_ring_strips_container) {
    for (auto iter=el_strips_ring->begin(); iter != el_strips_ring->end(); iter++){
      single_strips_ring.push_back(*iter);
    }
    if (single_strips_ring.size() == 170){
      m_offlineStripsRings->push_back(single_strips_ring);
      single_strips_ring.clear();
    }
  }

  ATH_MSG_DEBUG("Offline Strips Ringer Information Collected! That's good!");


  return StatusCode::SUCCESS;
}

StatusCode OfflineRingerDumperAlg::collectEventInfo(){
  const xAOD::EventInfo* ei = nullptr;
  ATH_CHECK( evtStore()->retrieve(ei,"EventInfo"));
  ATH_MSG_DEBUG("EventInfo Container Retrieved! That's good!");
  m_pileup =  ei->actualInteractionsPerCrossing();
  return StatusCode::SUCCESS;
}


StatusCode  OfflineRingerDumperAlg::collectOfflineSS(const xAOD::Electron *electron){
  el_f1->push_back(electron->showerShapeValue(xAOD::EgammaParameters::f1));
  el_f3->push_back(electron->showerShapeValue(xAOD::EgammaParameters::f3));
  el_eratio->push_back(electron->showerShapeValue(xAOD::EgammaParameters::Eratio));
  el_weta1->push_back(electron->showerShapeValue(xAOD::EgammaParameters::weta1));
  el_weta2->push_back(electron->showerShapeValue(xAOD::EgammaParameters::weta2));
  el_fracs1->push_back(electron->showerShapeValue(xAOD::EgammaParameters::fracs1));
  el_wtots1->push_back(electron->showerShapeValue(xAOD::EgammaParameters::wtots1));
  el_e277->push_back(electron->showerShapeValue(xAOD::EgammaParameters::e277));
  el_reta->push_back(electron->showerShapeValue(xAOD::EgammaParameters::Reta));
  el_rphi->push_back(electron->showerShapeValue(xAOD::EgammaParameters::Rphi));
  el_deltae->push_back(electron->showerShapeValue(xAOD::EgammaParameters::DeltaE));
  el_rhad->push_back(electron->showerShapeValue(xAOD::EgammaParameters::Rhad));
  el_rhad1->push_back(electron->showerShapeValue(xAOD::EgammaParameters::Rhad1));
   
  return StatusCode::SUCCESS;
}

StatusCode OfflineRingerDumperAlg::finalize() {
  ATH_MSG_INFO ("Finalizing " << name() << "...");
  return StatusCode::SUCCESS;
}


StatusCode OfflineRingerDumperAlg::collectOfflineInfo(){

//   const xAOD::ElectronContainer *offlineElectrons = nullptr;


//   ATH_CHECK(evtStore()->retrieve(offlineElectrons, "Electrons"));
//   ATH_MSG_DEBUG("Offline Electron Container Retrieved! That's good!");
//   ATH_MSG_DEBUG("looping over offline electrons");

//   for (auto *electron : *offlineElectrons){
   
//     ATH_MSG_DEBUG("eta: " << electron->eta() << " phi: " << electron->phi() << " e: " << electron->e());

//     el_energy->push_back ( electron->e() );
//     el_eta->push_back ( electron->eta() );
//     el_phi->push_back ( electron->phi() );
  
//     if (!collectOfflineSS(electron)) ATH_MSG_DEBUG("Impossible to collect offline SS variables");

//     if (m_doMC){

//       const xAOD::TruthParticle* mc = xAOD::TruthHelpers::getTruthParticle(*electron);
//       if(mc){
//         ATH_MSG_DEBUG("We have a electron truth particle ... ");

//         mc_et->push_back(mc->pt());
//         mc_eta->push_back(mc->eta());
//         mc_phi->push_back(mc->phi());
//         mc_origin->push_back(electron->auxdata<int>("truthOrigin"));
//         mc_type->push_back(electron->auxdata<int>("truthType"));
          
//             // store mc truth that have matching
//         mc_match_et->push_back(mc->pt());
//         mc_match_eta->push_back(mc->eta());
//         mc_match_phi->push_back(mc->phi());
//         mc_match_origin->push_back(electron->auxdata<int>("truthOrigin"));
//         mc_match_type->push_back(electron->auxdata<int>("truthType"));

//         el_truth_pt->push_back(electron->pt());
//         el_truth_eta->push_back(electron->eta());
//         el_truth_phi->push_back(electron->phi());
//         el_truth_f1->push_back(electron->showerShapeValue(xAOD::EgammaParameters::f1));
//         el_truth_f3->push_back(electron->showerShapeValue(xAOD::EgammaParameters::f3));
//         el_truth_eratio->push_back(electron->showerShapeValue(xAOD::EgammaParameters::Eratio));
//         el_truth_weta1->push_back(electron->showerShapeValue(xAOD::EgammaParameters::weta1));
//         el_truth_weta2->push_back(electron->showerShapeValue(xAOD::EgammaParameters::weta2));
//         el_truth_fracs1->push_back(electron->showerShapeValue(xAOD::EgammaParameters::fracs1));
//         el_truth_wtots1->push_back(electron->showerShapeValue(xAOD::EgammaParameters::wtots1));
//         el_truth_e277->push_back(electron->showerShapeValue(xAOD::EgammaParameters::e277));
//         el_truth_reta->push_back(electron->showerShapeValue(xAOD::EgammaParameters::Reta));
//         el_truth_rphi->push_back(electron->showerShapeValue(xAOD::EgammaParameters::Rphi));
//         el_truth_deltae->push_back(electron->showerShapeValue(xAOD::EgammaParameters::DeltaE));
//         el_truth_rhad->push_back(electron->showerShapeValue(xAOD::EgammaParameters::Rhad));
//         el_truth_rhad1->push_back(electron->showerShapeValue(xAOD::EgammaParameters::Rhad1));
//       }           
//     }
//   }    
//   ATH_MSG_DEBUG("Offline Electron Container Information Collected! That's good!");
  return StatusCode::SUCCESS;
}
  
 
