import ROOT
from ROOT import TFile
from ROOT import TCanvas, TH1F,TH2F, TH1I, TFile,TLine,TGraph, TGraph2D, TGraphErrors,TMultiGraph, THStack
from ROOT import TLatex, gPad, TLegend
from ROOT import kRed, kBlue, kBlack,TLine,kBird, kOrange,kGray, kYellow, kViolet, kGreen, kAzure
from ROOT import gROOT
from pandas import DataFrame
from array import array
import numpy as np
import matplotlib.pyplot as plt 

file = TFile('/eos/user/e/eegidiop/worktransf/offlineRinger/zee.mc21.stripsPhi.NTUPLE.new.pool.root')
tree = file.Get('events')

def norm1( data ):
      norms = np.abs( data.sum(axis=1) )
      norms[norms==0] = 1
      return data/norms[:,None]

def readStrips(tree,evtMax,limEvt=False):    
    vars = ['strips',
            'et',
            'energy',
            'eta',
            'phi',
            'reta',
            'rhad',
            'rphi',
            'weta2',
            'f1',
            'f3',
            'e277',
            'eratio']
    
    d = { key:[] for key in vars }        
    rings_strip_offline = []
    strip_rings_temp = []
    
    for eventIndex, event in enumerate(tree):        
        for k,strip in enumerate(event.offline_strip_rings):
            rings_strip_offline.append(list(event.offline_strip_rings)[k])
            d['et'].append(list(event.el_et)[k])   
            d['energy'].append(list(event.el_energy)[k])
            d['eta'].append(list(event.el_eta)[k])
            d['phi'].append(list(event.el_phi)[k])
            d['reta'].append(list(event.el_reta)[k])
            d['rphi'].append(list(event.el_rphi)[k])
            d['rhad'].append(list(event.el_rhad)[k])
            d['weta2'].append(list(event.el_weta2)[k])
            d['f1'].append(list(event.el_f1)[k])
            d['f3'].append(list(event.el_f3)[k])
            d['e277'].append(list(event.el_e2777)[k])
            d['eratio'].append(list(event.el_eratio)[k])
                    
        if (limEvt):
            if (eventIndex >= evtMax): break

    for i in range(len(rings_strip_offline[:])):
        for idx,st in enumerate([rings_strip_offline[i]][0]):
            strip_rings_temp.append(st)
        d['strips'].append(strip_rings_temp)
        strip_rings_temp = []
    
    return DataFrame(d)

print('-- ##### Reading DataFrame ##### -- ')
strip = readStrips(tree,20000,True)

zee_rings_et2 = strip.loc[(30<strip['et'])&(strip['et']<40)]
zee_rings_eta0_et2 = zee_rings_et2[(np.abs(strip['eta'])<0.8)]
strips_et2_eta0 = (zee_rings_eta0_et2['strips'].values).tolist()

x_axis = np.arange(169)+1
signal = norm1(np.array(strips_et2_eta0))
meanSignal = np.mean(signal,axis=0)
sSignal = np.std(signal,axis=0)

xS  = array( 'f', x_axis )
exS = 0
yS  = array( 'f', meanSignal)
eyS = array( 'f', sSignal)

print('-- ##### Plot Mean Profile Strips ##### -- ')
c1 = TCanvas( 'c1', '', 200, 10, 1600, 900 )
 
# c1.SetGrid()
c1.GetFrame().SetFillColor( 21 )
c1.GetFrame().SetBorderSize( 12 )
# c1.SetLogy()
gr = TGraphErrors( n, xS, yS, exS, eyS )

gr.SetLineColor( 4 )
gr.SetLineWidth( 2 )
gr.SetMarkerColor( 1 )
gr.SetMarkerStyle( 21 )
gr.SetMarkerSize( 1 )

gr.SetTitle()
gr.GetXaxis().SetTitle( 'Ring' )
gr.GetXaxis().SetTitleSize(.04)
gr.GetXaxis().SetLimits( 0,n )
gr.GetXaxis().SetLabelSize( 0.05 )

gr.GetYaxis().SetTitle( 'Normalized Energy' )
gr.GetYaxis().SetTitleSize(.05)
gr.GetYaxis().SetRangeUser(0.001, 0.4)
gr.GetYaxis().SetLabelSize( 0.05 )
gr.GetYaxis().SetTitleOffset(1)

gr.SetFillColor(kAzure)
gr.Draw()

#Layer lines
lineEM1 = TLine(14,1e-3,14,0.4)
lineEM1.SetLineStyle(5)
lineEM1.Draw()

lineEM2 = TLine(139,1e-3,139,0.4)
lineEM2.SetLineStyle(5)
lineEM2.Draw()

lineEM3 = TLine(153,1e-3,153,0.4)
lineEM3.SetLineStyle(5)
lineEM3.Draw()

lineH1 = TLine(160,1e-3,160,0.4)
lineH1.SetLineStyle(5)
lineH1.Draw()

# Legends
legend=TLegend(0.1,0.85)
legend.AddEntry ( gr , " Z#rightarrowee (Signal) ","l" )
legend.Draw()
legend.SetBorderSize(0)
legend.SetTextSize(0.06)
legend.SetFillStyle(0)

LZTLabel(c1, 0.1, 0.92, '')

# Legend Layers
AddTexLabel(c1, 0.11, 0.85, 'PS', textsize=0.03)
AddTexLabel(c1, 0.17, 0.85, 'EM1', textsize=0.03)
AddTexLabel(c1, 0.760, 0.85, 'EM2', textsize=0.03)
AddTexLabel(c1, 0.82, 0.85, 'EM3', textsize=0.03)
AddTexLabel(c1, 0.855, 0.85, 'Had.', textsize=0.03)
AddTexLabel(c1, 0.855, 0.805, 'Layers', textsize=0.03)
c1.Draw()

name_plot = 'offlineStrips_meanProfile_et2_eta0'
c1.SaveAs(name_plot)
