#ifndef FORWARDRINGERDUMPER_FORWARDRINGERDUMPERALG_H
#define FORWARDRINGERDUMPER_FORWARDRINGERDUMPERALG_H
#include "GaudiKernel/ToolHandle.h"
#include "xAODEventInfo/EventInfo.h"
#include "AthenaBaseComps/AthAlgorithm.h"
#include <TTree.h>
#include "GaudiKernel/ServiceHandle.h"
#include "GaudiKernel/ITHistSvc.h"
#include <xAODCaloRings/versions/RingSet_v1.h>
#include <xAODCaloRings/RingSetContainer.h>
#include <xAODEgamma/versions/Electron_v1.h>
#include <xAODEgamma/ElectronContainer.h>
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/xAODTruthHelpers.h"
#include <xAODCaloRings/versions/CaloRings_v1.h>
#include <xAODCaloRings/CaloRingsContainer.h>
#include "xAODCaloRings/tools/getCaloRingsDecorator.h" 
#include "EgammaAnalysisInterfaces/IAsgForwardElectronIsEMSelector.h"
#include "AsgTools/AsgTool.h"


using namespace std;

class ForwardRingerDumperAlg: public ::AthAlgorithm { 

 public: 
    ForwardRingerDumperAlg( const std::string& name, ISvcLocator* pSvcLocator );
    virtual ~ForwardRingerDumperAlg(); 

    virtual StatusCode  initialize();     //once, before any input is loaded
    virtual StatusCode  execute();        //per event
    virtual StatusCode  finalize();       //once, after all events processed
    void                clear();
    void                bookBranches(TTree *tree);
    virtual StatusCode  collectInfo();
    virtual StatusCode  collectRingsInfo();
    virtual StatusCode  collectEventInfo();
    virtual StatusCode  collectElectronInfo();
    virtual StatusCode  collectOfflineSS(const xAOD::Electron *electron);
    bool getCaloRings( const xAOD::Electron_v1 *, std::vector<float> & );
    bool getApproval (const xAOD::Electron_v1 * , const std::string pidname);

  private: 
    TTree *m_Tree;

    ServiceHandle<ITHistSvc> m_ntsvc;
    Gaudi::Property<bool>                          m_doMC       {this, "doMC"     , false ,  "Dump MC information"};
    Gaudi::Property<bool>                          m_dumpCells  {this, "dumpCells", false ,  "Dump Cell information"};

    ToolHandleArray<IAsgForwardElectronIsEMSelector> m_electronIsEMTool;
            
    float                                 m_pileup          = -999;
    std::vector < std::vector < float > > *m_cells_eta      = nullptr;
    std::vector < std::vector < float > > *m_cells_phi      = nullptr;
    std::vector < std::vector < float > > *m_cells_et       = nullptr;
    std::vector < std::vector < int > >   *m_cells_sampling = nullptr;
    std::vector < std::vector <int> >     *m_cells_ringNumber = nullptr;
    std::vector < std::vector<float> >    *m_cells_seedEta   = nullptr;
    std::vector < std::vector<float> >    *m_cells_seedPhi   = nullptr;
    std::vector < float >                 *m_clusterEnergy  = nullptr;
    std::vector < float >                 *m_clusterEta     = nullptr;
    std::vector < float >                 *m_clusterPhi     = nullptr;
    std::vector < float >                 *m_clustere237    = nullptr;
    std::vector < float >                 *m_clustere277    = nullptr;
    std::vector < float >                 *m_clusterfracs1  = nullptr;
    std::vector < float >                 *m_clusterweta2   = nullptr;
    std::vector < float >                 *m_clusterwstot   = nullptr;
    std::vector < float >                 *m_clusterehad1   = nullptr;
    std::vector < float > *m_offlineRings   = nullptr;
    std::vector < float > *el_energy = nullptr;
    std::vector < float > *el_et = nullptr;
    std::vector < float > *el_eta = nullptr;
    std::vector < float > *el_phi = nullptr;
    std::vector < float > *el_f1  = nullptr;
    std::vector < float > *el_f3  = nullptr;
    std::vector < float > *el_eratio = nullptr;
    std::vector < float > *el_weta1 = nullptr;  
    std::vector < float > *el_weta2 = nullptr;  
    std::vector < float > *el_fracs1 = nullptr; 
    std::vector < float > *el_wtots1 = nullptr; 
    std::vector < float > *el_e277 = nullptr; 
    std::vector < float > *el_reta = nullptr; 
    std::vector < float > *el_rphi = nullptr; 
    std::vector < float > *el_deltae = nullptr; 
    std::vector < float > *el_rhad = nullptr;  
    std::vector < float > *el_rhad1 = nullptr;  
    std::vector<bool> *m_hasCalo = nullptr;
    std::vector < float > *m_el_calo_et = nullptr;
    std::vector < float > *m_el_calo_eta = nullptr;
    std::vector < float > *m_el_calo_phi = nullptr;
    std::vector < float > *m_el_calo_etaBE2 = nullptr;
    std::vector < float > *m_el_calo_e = nullptr;
    std::vector < float > *m_el_tight = nullptr;
    std::vector < float > *m_el_medium = nullptr;
    std::vector < float > *m_el_loose = nullptr;
    std::vector < float > *mc_eta = nullptr;
    std::vector < float > *mc_phi = nullptr;
    std::vector < float > *mc_et = nullptr;
    std::vector < int > *mc_origin = nullptr;
    std::vector < int > *mc_type = nullptr;
}; 


#endif //> !FORWARDRINGERDUMPER_FORWARDRINGERDUMPERALG_H
