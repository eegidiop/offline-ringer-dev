#ifndef RINGERDUMPER_PHOTONRINGERDUMPERALG_H
#define RINGERDUMPER_PHOTONRINGERDUMPERALG_H
#include "GaudiKernel/ToolHandle.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODTrigRinger/versions/TrigRingerRings_v2.h"
#include "xAODTrigRinger/TrigRingerRingsContainer.h"
#include "AthenaBaseComps/AthAlgorithm.h"
#include <TTree.h>
#include "GaudiKernel/ServiceHandle.h"
#include "GaudiKernel/ITHistSvc.h"
#include <xAODCaloRings/versions/RingSet_v1.h>
#include <xAODCaloRings/RingSetContainer.h>
#include <xAODEgamma/versions/Electron_v1.h>
#include <xAODEgamma/ElectronContainer.h>
#include "xAODMuon/MuonContainer.h"
#include "xAODMuon/MuonAuxContainer.h"
#include <xAODEgamma/versions/Photon_v1.h>
#include <xAODEgamma/PhotonContainer.h>
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/xAODTruthHelpers.h"
#include <xAODCaloRings/versions/CaloRings_v1.h>
#include <xAODCaloRings/CaloRingsContainer.h>


using namespace std;

class PhotonRingerDumperAlg: public ::AthAlgorithm { 

 public: 
    PhotonRingerDumperAlg( const std::string& name, ISvcLocator* pSvcLocator );
    virtual ~PhotonRingerDumperAlg(); 

    virtual StatusCode  initialize();     //once, before any input is loaded
    virtual StatusCode  execute();        //per event
    virtual StatusCode  finalize();       //once, after all events processed
    void                clear();
    void                bookBranches(TTree *tree);
    
    virtual StatusCode  selectElectrons();
    virtual StatusCode  selectMuons();
    virtual StatusCode  selectPhotons();


    virtual StatusCode  collectInfo();
    virtual StatusCode  collectRingsInfo();
    virtual StatusCode  collectEventInfo();
    virtual StatusCode  collectOfflineInfo();
    virtual StatusCode  collectOfflineSS(const xAOD::Photon *photon);

  private: 
    TTree *m_Tree;

    ServiceHandle<ITHistSvc> m_ntsvc;
    Gaudi::Property<bool>                          m_doMC             {this, "doMC"     , false ,  "Dump MC information"};
    Gaudi::Property<bool>                          m_dumpOfflineRings {this, "dumpOfflineRings", false ,  "Dump Offline Rings"};
    Gaudi::Property<bool>                          m_dumpCells        {this, "dumpCells", false ,  "Dump Cell information"};
    Gaudi::Property<bool>                          m_doZrad           {this, "doZrad", false ,  "Do Zrad tag and probe"};
    std::vector < xAOD::Electron >        *m_selectedElectrons;
    std::vector < xAOD::Muon >            *m_selectedMuons;
    std::vector < xAOD::Photon >          *m_selectedPhotons;
    float                                 m_pileup          = -999;
    std::vector < float >                 *m_clusterEnergy  = nullptr;
    std::vector < float >                 *m_clusterEta     = nullptr;
    std::vector < float >                 *m_clusterPhi     = nullptr;
    std::vector < float >                 *m_clustere237    = nullptr;
    std::vector < float >                 *m_clustere277    = nullptr;
    std::vector < float >                 *m_clusterfracs1  = nullptr;
    std::vector < float >                 *m_clusterweta2   = nullptr;
    std::vector < float >                 *m_clusterwstot   = nullptr;
    std::vector < float >                 *m_clusterehad1   = nullptr;
    std::vector < std::vector < float > > *m_rings          = nullptr;
    std::vector < std::vector < float > > *m_offlineRings   = nullptr;
    std::vector < std::vector < float > > *m_cells_eta      = nullptr;
    std::vector < std::vector < float > > *m_cells_phi      = nullptr;
    std::vector < std::vector < float > > *m_cells_et       = nullptr;
    std::vector < std::vector < int > >   *m_cells_sampling = nullptr;
    std::vector < std::vector < int > >   *m_cells_size     = nullptr;
    std::vector < std::vector < double > >*m_rings_sum     = nullptr;
    std::vector < float > *ph_energy = nullptr;
    std::vector < float > *ph_eta = nullptr;
    std::vector < float > *ph_phi = nullptr;
    std::vector < float > *ph_f1  = nullptr;
    std::vector < float > *ph_f3  = nullptr;
    std::vector < float > *ph_eratio = nullptr;
    std::vector < float > *ph_weta1 = nullptr;  
    std::vector < float > *ph_weta2 = nullptr;  
    std::vector < float > *ph_fracs1 = nullptr; 
    std::vector < float > *ph_wtots1 = nullptr; 
    std::vector < float > *ph_e277 = nullptr; 
    std::vector < float > *ph_reta = nullptr; 
    std::vector < float > *ph_rphi = nullptr; 
    std::vector < float > *ph_deltae = nullptr; 
    std::vector < float > *ph_rhad = nullptr;  
    std::vector < float > *ph_rhad1 = nullptr;  
    std::vector < float > *mc_eta = nullptr;
    std::vector < float > *mc_phi = nullptr;
    std::vector < float > *mc_et = nullptr;
    std::vector < int > *mc_origin = nullptr;
    std::vector < int > *mc_type = nullptr;
}; 


#endif //> !RINGERDUMPER_PHOTONRINGERDUMPERALG_H
