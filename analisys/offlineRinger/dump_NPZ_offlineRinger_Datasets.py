import ROOT
from ROOT import TFile
from ROOT import TCanvas, TH1F,TH2F, TH1I, TFile,TLine,TGraph, TGraph2D, TGraphErrors,TMultiGraph, THStack
from ROOT import TLatex, gPad, TLegend
from ROOT import kRed, kBlue, kBlack,TLine,kBird, kOrange,kGray, kYellow, kViolet, kGreen, kAzure
from ROOT import gROOT
from array import array
import numpy as np
import os 

#getting input offline ringer ntuple
path_to_file = '/afs/cern.ch/work/e/eegidiop/offRingerDev/offline-ringer-dev/offlineDumper/run/user.eegidiop.mc21.601189.PhPy8EG_AZNLO_Zee.recon.HIST.e8392_e7400_s3775_r13614.pool.root'
file = TFile(path_to_file)
tree = file.Get('events')

#Resources
def norm1( data ):
      norms           = np.abs( data.sum(axis=1) )
      norms[norms==0] = 1
      return data/norms[:,None]

def readData(tree,evtMax,etBin,etaBin,limEvt=False):    
    vars = ['rings',
            'quarter',
            'strips',
            'et',
            'energy',
            'eta',
            'phi',
            'reta',
            'rhad',
            'rphi',
            'weta2',
            'f1',
            'f3',
            'e277',
            'eratio']
    
    d = { key:[] for key in vars }      

    rings_std_offline     = []
    std_rings_temp        = []
    
    rings_strip_offline   = []
    strip_rings_temp      = []
    
    rings_quarter_offline = []
    quarter_rings_temp    = []
    
    nFloats               = 8
 
    m_doEnergy = True   
    for eventIndex, event in enumerate(tree):        
        for k,strip in enumerate(event.offline_strip_rings):
            
            #Binning
            if (m_doEnergy):
                if not(etBin[0]  < list(event.el_energy)[k]  < etBin[1] ): continue
                if not(etaBin[0] < list(event.el_eta)[k] < etaBin[1]): continue
            else:
                if not(etBin[0]  < list(event.el_et)[k]  < etBin[1] ): continue
                if not(etaBin[0] < list(event.el_eta)[k] < etaBin[1]): continue
            
            d['et'].append(list(event.el_et)[k])   
            d['energy'].append(list(event.el_energy)[k])
            d['eta'].append(list(event.el_eta)[k])
            d['phi'].append(list(event.el_phi)[k])
            d['reta'].append(list(event.el_reta)[k])
            d['rphi'].append(list(event.el_rphi)[k])
            d['rhad'].append(list(event.el_rhad)[k])
            d['weta2'].append(list(event.el_weta2)[k])
            d['f1'].append(list(event.el_f1)[k])
            d['f3'].append(list(event.el_f3)[k])
            d['e277'].append(list(event.el_e2777)[k])
            d['eratio'].append(list(event.el_eratio)[k])
            
            rings_std_offline.append(list(np.around((event.offline_std_rings)[k],nFloats)))
            rings_strip_offline.append(list(np.around((event.offline_strip_rings)[k],nFloats)))
            rings_quarter_offline.append(list(np.around((event.offline_asym_rings)[k],nFloats)))          
                    
        if (limEvt):
            if (eventIndex >= evtMax): break
    
    for i in range(len(rings_strip_offline[:])):
        for idx,st in enumerate([rings_strip_offline[i]][0]):
            strip_rings_temp.append(st)
        d['strips'].append(strip_rings_temp)
        strip_rings_temp = []
    
    for i in range(len(rings_std_offline[:])):
        for idx,st in enumerate([rings_std_offline[i]][0]):
            std_rings_temp.append(st)
        d['rings'].append(std_rings_temp)
        std_rings_temp = []
        
    for i in range(len(rings_quarter_offline[:])):
        for idx,st in enumerate([rings_quarter_offline[i]][0]):
            quarter_rings_temp.append(st)
        d['quarter'].append(quarter_rings_temp)
        quarter_rings_temp = []
    
    return d

# Dump NPZ datasets

# Offline phase space binning

etBin  = [[15 , 20] ,[20, 30]  ,[30, 40]  ,[40, 50],[50,1000000]]
etaBin = [[0.00, 0.80], [0.80, 1.37], [1.37,1.54],[1.54,2.50]]

# Limit events ?
limitEvt = False
nEvt = 0
if limitEvt:
    nEvt = 1000

#go to the desired folder
os.mkdir('./dataset')
os.chdir('./dataset')

for i,etBins in enumerate(etBin):
    for j,etaBins in enumerate(etaBin):

        print("Dump NPZ files for Phase Space - ET: "+str(i)+" "+str(etBins)+" eta: "+str(j)+" "+str(etaBins))
        collections = readData(tree,nEvt,etBins,etaBins,limitEvt)    
        np.savez("user.eegidiop.mc21.601189.PhPy8EG_AZNLO_Zee.recon.e8392_e7400_s3775_r13614.et%i_eta%i"%(i,j), **collections)
